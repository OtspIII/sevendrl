﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    public GameObject LoadingScreen;
    
    // Start is called before the first frame update
    void Start()
    {
       IM.Init(); 
    }

    // Update is called once per frame
    void Update()
    {
        if (IM.GetButtonDown(Inputs.ChooseAction))
        {
            PlayerState.Wipe();
            LoadingScreen.gameObject.SetActive(true);
            SceneManager.LoadScene("Gameplay");
        }
    }
}
