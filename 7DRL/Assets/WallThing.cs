﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WallThing : WorldThing
{
    public RectInt Size = new RectInt();
    public TextMeshPro BodP;
    public bool AutoBuild;
    
    public override void OnStart()
    {
        base.OnStart();
        if (AutoBuild)
            Build(Size);
    }

    public void Build(RectInt shape)
    {
        Size = shape;
        Vector3 mid = new Vector3(Size.x + (((float)Size.width-1) / 2),Size.y + (((float)Size.height-1) / 2),50);
        transform.position = mid;
        for (int y = Size.y; y < Size.y + Size.height; y++)
        {
            for (int x = Size.x; x < Size.x + Size.width; x++)
            {
                TextMeshPro t = Instantiate(BodP, new Vector3(x, y, 50), Quaternion.identity);
                t.text = "#";
                t.transform.SetParent(transform);
            }
        }

        ((BoxCollider2D) C).size = new Vector2(Size.width,Size.height);
    }
}
