﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActionCard : MonoBehaviour
{
    public ActTracker Act;
    public TextMeshPro Text;
    public TextMeshPro Desc;
    public int N;
    public SpriteRenderer BG;
    public Color ActiveC;
    public Color PassiveC;

    public void Imprint(ActTracker act, int n)
    {
        Act = act;
        Text.text = (n + 1) + ") " + act.Act.Name;
        string desc = act.GetDesc(God.GM.Player);
//        if (act.MaxUses >= 0)
//        {
//            desc = God.GM.Player.KnownActions[act] + "/" + act.MaxUses;
//        }
        Desc.text = desc;
        N = n;
        BG.color = God.GM.PlayerAct == Act ? PassiveC : ActiveC;
    }

    void Update()
    {
        if (IM.GetButtonDown(God.KeyNums[N]))
        {
            Activate();
        }

        BG.color = God.GM.PlayerAct == Act ? PassiveC : ActiveC;
    }
    
    private void OnMouseUp()
    {
        Activate();
    }

    public void Activate()
    {
        God.GM.PlayerAct = Act;
        God.GM.JustClicked = true;
        God.GM.SetMode(GM.GameMode.ChooseAction);
//        Debug.Log("ACTIVATE: " + Act.Name);
//        if (!full)
//            return;
//        God.GM.Player.SetState(Act);
    }
}
