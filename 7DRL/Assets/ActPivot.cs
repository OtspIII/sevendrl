﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActPivot : MonoBehaviour
{
    public TextMeshPro Text;
    public BoxCollider2D Collider;
    public Actor Who;

    public void Imprint(string txt, float x=1,float y=1)
    {
        Vector2 size = new Vector2(x,y);
        Imprint(txt,size);
    }
    
    public void Imprint(string txt, Vector2 size)
    {
        Text.text = txt;
        Collider.size = size;
        Collider.offset = new Vector2(size.x / 2,0);
        gameObject.SetActive(true);
    }

    public void Rotate(Vector2 targ,float lerp=1)
    {
        Rotate(God.LookAtRot(Who.transform, targ),lerp);
    }
    
    public void Rotate(float r,float lerp=1)
    {
        if (lerp < 1)
            r = Mathf.LerpAngle(transform.rotation.eulerAngles.z, r, lerp);
        transform.rotation = Quaternion.Euler(new Vector3(0,0,r));
    }
}
