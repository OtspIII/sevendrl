﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ProjectileTrait : Trait
{
    public ProjectileTrait()
    {
        AddListen(GMsgs.Reflect);
    }

    public override void GetMsg(Actor who, GMsg msg)
    {
        switch (msg.Type)
        {
            case GMsgs.Reflect:
            {
                float spd = who.WalkMove.magnitude;
                Vector2 dir = who.transform.position - msg.Source.transform.position;
                dir = dir.normalized * spd;
                who.WalkMove = dir;//*= -1;
                who.Faction = Factions.Player;
                who.Parent = null;
                return;
            }

        }
    }
}

public class AliveTrait : Trait
    {
        public AliveTrait()
        {
            AddListen(GMsgs.TakeDamage);
            AddListen(GMsgs.TakeAction);
            AddListen(GMsgs.Die);
            AddListen(GMsgs.Petrify);
            AddListen(GMsgs.Teleport);
            AddListen(GMsgs.Heal);
            AddListen(GMsgs.Invisible);
            AddListen(GMsgs.Anger);
            AddListen(GMsgs.Polymorph);
        }

        public override void Setup(Actor who, Actor source = null)
        {
            base.Setup(who, source);
            who.AddTag(Tags.Creature);
        }

        public override void GetMsg(Actor who, GMsg msg)
        {
            switch (msg.Type)
            {
                case GMsgs.TakeDamage:
                {
                    who.ChangeStat(Stats.HP, -msg.Amount);
                    God.GM.SpawnHeadtext(who, msg.Amount.ToString(), Colors.Red);
//                Debug.Log("TAKE DAMAGE: " + who + " / " + msg.Amount + " / " + who.GetStat(Stats.HP) + " / " + msg.Act);
                    if (who.GetStat(Stats.HP) <= 0)
                    {
                        new GMsg(GMsgs.Die).Send(who, msg.Source, msg.Act);
                    }

                    if (msg.Source != null && !who.HasTag(Tags.Beefy))
                    {
                        if (msg.Tags.Contains(MTags.LowKnockback))
                            who.SetAct(new KnockbackAction(0.15f, 0.3f, msg.Source.transform.position));
                        else
                            who.SetAct(new KnockbackAction(0.5f, 2, msg.Source.transform.position));
                    }

                    return;
                }
                case GMsgs.TakeAction:
                {
                    msg.ForceAct.TargetV = msg.Source.transform.position;
                    who.SetAct(msg.ForceAct);
                    return;
                }
                case GMsgs.Die:
                {
                    if (God.GM.Player != null)
                        God.GM.Player.SetStat(Stats.ST, God.GM.Player.GetStat(Stats.MaxST));
                    God.GM.Spawn(Things.Corpse, who.transform.position);
                    who.Destruct();
                    return;
                }
                case GMsgs.Petrify:
                {
                    God.GM.SpawnHeadtext(who, "petrified", Colors.Gray);
                    if (God.GM.Player != null)
                        God.GM.Player.SetStat(Stats.ST, God.GM.Player.GetStat(Stats.MaxST));
                    string bod = who.Body.text;
                    Actor corpse = God.GM.Spawn(Things.Corpse, who.transform.position);
                    corpse.Body.text = bod;
                    corpse.Name = "statue of " + who.Name;
                    who.Destruct();
                    return;
                }
                case GMsgs.Teleport:
                {
                    God.GM.SpawnHeadtext(who,"teleported",Colors.Cyan);
                    Tile t = God.GM.D.NonHalls.Random().RandomTile();
                    if (t == null)
                        return;
                    who.transform.position = t.Loc.ToVector();
                    return;
                }
                case GMsgs.Invisible:
                {
                    God.GM.SpawnHeadtext(who,"invisible",Colors.Gray);
                    if (who == God.GM.Player)
                        return;
                    who.DefaultColor = Color.clear;
                    who.SetColor(Colors.None);
                    return;
                }
                case GMsgs.Anger:
                {
                    if (who == God.GM.Player)
                        return;
                    God.GM.SpawnHeadtext(who,"angry",Colors.Red);
                    who.DefaultColor = Color.red;
                    who.SetColor(Colors.None);
                    who.Faction = Factions.None;
                    return;
                }
                case GMsgs.Polymorph:
                {
                    if (who == God.GM.Player)
                        return;
                    Vector2 where = who.transform.position;
                    God.GM.SpawnHeadtext(who,"polymorphed",Colors.Pink);
                    who.Destruct();
                    Actor p = God.GM.Spawn(Library.RandomMonster(), where);
                    p.Faction = Factions.None;
                    p.DefaultColor = Color.red;
                    p.SetColor(Colors.None);
                    return;
                }
                case GMsgs.Heal:
                {
                    float amt = msg.Amount;
                    amt = Mathf.Min(amt, who.GetStat(Stats.MaxHP) - who.GetStat(Stats.HP));
                    who.ChangeStat(Stats.HP, amt);
                    God.GM.SpawnHeadtext(who, amt.ToString(), Colors.Green);
                    return;
                }
            }
        }
    }

public class PlayerTrait : Trait
{
    public PlayerTrait()
    {
        AddListen(GMsgs.Inputs);
        AddListen(GMsgs.Stairs);
        AddListen(GMsgs.GetGear);
        AddListen(GMsgs.WinGame);
        AddListen(GMsgs.GetCoins);
        AddListen(GMsgs.Die);
        AddListen(GMsgs.Petrify);
        AddListen(GMsgs.Invisible);
    }

    public override void Setup(Actor who, Actor source = null)
    {
        base.Setup(who, source);
        God.GM.SetPlayer(who);
    }

    public override void GetMsg(Actor who, GMsg msg)
    {
//        Debug.Log("B: " + who + " / " + msg.Type);
        switch (msg.Type)
        {
            case GMsgs.Inputs:
            {
                Vector2 move = Vector2.zero;
                if (who.CanAct())
                {
                    if (IM.GetButton(Inputs.MoveRight))
                        move.x += who.GetStat(Stats.SP);
                    if (IM.GetButton(Inputs.MoveLeft))
                        move.x -= who.GetStat(Stats.SP);
                    if (IM.GetButton(Inputs.MoveUp))
                        move.y += who.GetStat(Stats.SP);
                    if (IM.GetButton(Inputs.MoveDown))
                        move.y -= who.GetStat(Stats.SP);

                    if (IM.GetButtonDown(Inputs.ChooseAction) && God.GM.Mode == GM.GameMode.Default)
                        God.GM.SetMode(GM.GameMode.ChooseAction);
                    else if (IM.GetButtonDown(Inputs.ChooseAction) && God.GM.Mode == GM.GameMode.ChooseAction)
                        God.GM.SetMode(GM.GameMode.Default);
                }

                who.WalkMove = Vector2.Lerp(who.WalkMove, move, 0.4f);

                return;
            }
            case GMsgs.Stairs:
            {
                PlayerState.Floor++;
                God.GM.LoadScene("Gameplay");
                return;
            }
            case GMsgs.WinGame:
            {
                PlayerState.Score +=1000;
                God.GM.ShowMessage("Score: " + (int) PlayerState.Score,
                    "You Win!\n\nCongratulations, you have claimed the Amulet of Yendor and escaped the dungeon.",
                    true);
                return;
            }
            case GMsgs.GetGear:
            {
                if (who.KnownActions.Count >= God.KeyNums.Length)
                    return;
                CharAction act = msg.Source.Gear.GetAction();
                act.MaxUses = Random.Range(act.Uses.x, act.Uses.y + 1);
//                act.Reward = God.WeightedRandom(Rewards);
                who.AddAction(act);
                msg.Source.Destruct();
                return;
            }
            case GMsgs.Die:
            {
                God.GM.ShowMessage("SCORE: " + PlayerState.Score,
                    "You have died!\n\nFloor: " + PlayerState.Floor + "\nCause: " +
                    msg.Act.GetDeathDesc(msg.Source), true);
                return;
            }
            case GMsgs.Petrify:
            {
                God.GM.ShowMessage("SCORE: " + PlayerState.Score,
                    "You have died!\n\nFloor: " + PlayerState.Floor + "\nCause: " +
                    msg.Act.GetDeathDesc(msg.Source), true, -5f);
                return;
            }
            case GMsgs.GetCoins:
            {
                PlayerState.Score += msg.Source.GetStat(Stats.MaxST);
                msg.Source.Destruct();
                return;
            }
            case GMsgs.Invisible:
            {
                who.SetAct(new BeInvisibleAction(10));
                return;
            }
        }
    }
}