﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;

public class CharAction
{
    public string Name
    {
        get { return GetName(); }
        set { N = value; }
    }
    private string N;
    public WorldThing Target;
    public Vector2 TargetV;
    public bool AllowMove = false;
    public float Range = 5;
    public Vector2 PivotSize = new Vector2(0,0);
    public float Duration;
    public float T;
    public Vector2 Loc;
    public float Clock;
    public bool PlayerAction = false;
    public bool WithinRange = false;
    public int MaxUses = -1;
    public float StaminaCost = 0;
    public List<Actor> Hit = new List<Actor>();
//    public RewardEntry Reward;
    public string Desc = "";
    public float ResolveSpeed = -1;
    public Point Uses = Point.Zero;
    public string Symbol = "==>";
    public float SymbolSize = 1;
    public string DeathDesc = "$";

    public virtual string GetName()
    {
        return N;
    }
    
    public virtual void Start(Actor who,Actor targ)
    {
        Hit.Clear();
        T = 0;
        Clock = 0;
        Loc = who.transform.position;
        PlayerAction = who == God.GM.Player || who.HasTag(Tags.Timeless);
        WithinRange = false;
        Target = null;
        if (StaminaCost != 0)
        {
            who.ChangeStat(Stats.ST, -StaminaCost);
//            Debug.Log("COST STAM: " + this + " / " + StaminaCost);
        }
    }
    
    public virtual void Run(Actor who)
    {
        Clock += God.GetTime(PlayerAction);
    }

    public virtual void OnTouch(Actor who, Actor other)
    {
        
    }
    
    public virtual void OnHit(Actor who, Actor other)
    {
        
    }
    
    public virtual void OnBump(Actor who, WorldThing other)
    {
        
    }

    public virtual bool End(Actor who, bool forced = false)
    {
        if (God.GM.PlayerAct?.Act == this && God.GM.PlayerAct.Uses == 0)
            God.GM.PlayerAct = null;
        return true;
    }

    public float GetRange(Actor who,float movementMult=0)
    {
        float rng = Range;
        if (movementMult > 0)
            rng = who.GetStat(Stats.SP) * movementMult;
        if (StaminaCost == 0)
            return rng;
        return rng * who.GetEnergy();
    }

    public string GetDeathDesc(Actor killer)
    {
        string r = DeathDesc;
        if (killer != null)
            r = r.Replace("$", killer.GetName(true));
        else
            r = r.Replace("$", "");
        return r;
    }

    public bool Recover(Actor who, float t,bool clock=true)
    {
        if ((clock && Clock >= Duration) || (!clock && T >= 1))
        {
            who.SetAct(new RecoverAction(t));
            return true;
        }

        return false;
    }

    public virtual void Preview(Actor who)
    {
        if (Range == 0)
            return;
        Vector2 range = God.CropRange(who, God.MouseLoc(),GetRange(who));
        float size = PivotSize.x;
        if (size != 0)
            size += 0.5f;
        God.GM.SetCursor(range,size * SymbolSize);
    }

    public virtual Vector2 GetTarget()
    {
        Vector2 r = TargetV;
        if (Target != null)
            r = Target.transform.position;
        return r;
    }

    public override string ToString()
    {
        string r = base.ToString();
        if (Name != null)
            r = Name;
        r += "[" + Clock + "/" + Duration + "]";
        return r;
    }

    public virtual void SetTarget(Actor who,bool moving)
    {
        Target = null;
        if (who.Target != null)
        {
            if (moving)
                Target = who.Target;
            else
                TargetV = God.CropRange(who,who.Target.transform.position,GetRange(who));
        }
        else
            TargetV = who.transform.position;
    }


    
}

public class RodAction : CharAction
{
    public RodEntry Rod;
    
    public override string GetName()
    {
        return Rod.GetName();
    }
    
    public override void Preview(Actor who)
    {
        if (!Rod.Identified)
        {
            Vector2 range = God.CropRange(who, God.MouseLoc(),5);
            God.GM.SetCursor(range,0);
            return;
        }
        base.Preview(who);
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        Rod.Identified = true;
    }
}

public class IdleAction : CharAction
{
    public IdleAction()
    {
        AllowMove = true;
    }
}

public class WanderAction : CharAction
{
    
    public WanderAction()
    {
        AllowMove = true;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        Vector2 pos = who.transform.position;
        TargetV = pos + new Vector2(Random.Range(-7,7),Random.Range(-7,7));
    }
    
    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Walk(this,who,0.5f);
        CA.LookAround(this,who);
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        who.SetAct(null);
    }
}


public class ActTracker
{
    public int Uses;
    public RewardEntry Reward;
    public CharAction Act;

    public ActTracker(CharAction act,int uses)
    {
        Act = act;
        Uses = uses;
        Reward = God.WeightedRandom(Library.Rewards);
//        Debug.Log("USES: " + uses + " / " + Act.GetName());
    }
    
    public void UsedUp(Actor who)
    {
        if (Reward != null)
        {
            who.ChangeStat(Reward.S, Reward.Amt);
        }
    }
    
    public string GetDesc(Actor who)
    {
        string r = Act.Desc;
        if (Act.StaminaCost > 0)
        {
            if (r != "") r += "\n";
            r += "COST: " + Act.StaminaCost + " ST";
        }

        if (Uses > 0)
        {
            if (r != "") r += "\n";
            r += "USES:" + Uses;// + "/" + MaxUses;
            if (Reward != null)
                r += " > " + God.Sign(Reward.Amt) + Reward.S;
        }
        
        return r;
    }
}

public class BeLobbedAction : CharAction
{
    private float MoveSpd = 30;
    private float Distance = 0;
    Vector2 Dir = Vector2.down;
    private Vector2 LastPos;
    public float Splash;
    
    public BeLobbedAction(Vector2 targ,float splash,PotionEntry pot=null)
    {
        AllowMove = true;
        TargetV = targ;
        Range = 10;
        Splash = splash;
        if (pot != null)
            DeathDesc = pot.GetName();
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        LastPos = who.transform.position;
        TargetV = God.CropRange(who,TargetV,GetRange(who));
//        if (targ != null)
//            TargetV = targ.transform.position;
//        else
//            TargetV = God.MouseLoc();

        Dir = (TargetV - LastPos).normalized;
        Distance =  Vector2.Distance(TargetV,who.transform.position);
        MoveSpd *= Distance / Range;
        who.WalkMove = Dir * MoveSpd;
    }
    
    public override void Run(Actor who)
    {
        base.Run(who);
        Vector2 pos = who.transform.position;
//        Vector2 move = Dir * MoveSpd;
//        who.RB.MovePosition(pos + move);
//        float amt = Vector2.Distance(pos,who.transform.position);
        Distance -= Vector2.Distance(pos, LastPos);
        LastPos = pos;
        if (Distance <= 0)
            who.SetAct(null);
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        who.SetAct(null);
    }

    public override bool End(Actor who, bool forced = false)
    {
        List<Actor> hit = God.GM.SpawnExplosion(who.transform.position,Factions.None,Splash);
        foreach (Actor a in hit)
        {
//            Debug.Log("HIT: " + a + " / " + who + " / " + who.Payload.Count);
            foreach (GMsg m in who.Payload)
            {
//                Debug.Log("MSG: " + a + " / " + m.Type + " / " + m.Amount);
                new GMsg(m).Send(a,who,this);
            }
        }
        who.Destruct();
        return base.End(who, forced);
    }
}

public class BeInvisibleAction : CharAction
{
    public BeInvisibleAction(float dur)
    {
        AllowMove = true;
        Duration = dur;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        who.AddTag(Tags.Invisible);
        who.SetColor(Colors.Clear,0.5f);
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        float remain = Duration - Clock;
        if (remain < 1)
        {
            if (remain%0.2f < 0.1f)
                who.SetColor(Colors.None);
            else
                who.SetColor(Colors.Clear,0.5f);
        }
        if (remain <=0)
            who.SetAct(null);
    }

    public override bool End(Actor who, bool forced = false)
    {
        who.TagList.Remove(Tags.Invisible);
        who.SetColor(Colors.None);
        return base.End(who, forced);
    }
}