﻿using System;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.XR.WSA.Input;
using Debug = UnityEngine.Debug;

public class Trait
{
	public Traits Type;

//	public MobileWT Who;
	public List<GMsgs> ListenFor = new List<GMsgs>();
	public string DefaultDuration = "0";
	public Dictionary<GMsgs,int> WatchFor = new Dictionary<GMsgs, int>();
//	public Dictionary<GEvents,Priorities> PriorityOverride = new Dictionary<GEvents, Priorities>();
//	public List<Traits> NeededTraits = new List<Traits>();
	public string TagDesc = "";
	public string HoverDesc = "";
	public string Aura = "";
	public bool AfterTurn = false;

	public virtual void Setup(Actor who,Actor source=null)
	{
	}
	
	public virtual void OnRemove(Actor who)
	{
		
	}

//	public virtual void AddMoreTrait(WorldThing who, int amt)
//	{
//		who.TraitList[Type].Amount += amt;
//	}

	public void AddListen(GMsgs type)
	{
		ListenFor.Add(type);
//		if (p != Priorities.None)
//			PriorityOverride.Add(type,p);
	}

	public virtual void GetMsg(Actor who, GMsg msg)
	{
		
	}
	
	public virtual void SeeHappen(Actor who, GMsg msg)
	{
		
	}

}

public enum Traits
{
	None,
}