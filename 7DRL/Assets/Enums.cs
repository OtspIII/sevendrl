﻿using System.Runtime.CompilerServices;
using UnityEngine;
public enum Things
{
    None=0,
    Player=1,
    Gear=2,
    Projectile=3,
    Grass=5,
    Stairs=6,
    Corpse=7,
    Amulet=8,
    Gold=9,
    SlimeTrail=10,
    FireTrail=11,
    
    Archer=101,
    Beetle=102,
    Cockatrice,
    Dragon,
    Elf,
    FlameElemental,
    Goblin=107,
    Hellhound,
    IceElemental,
    Jackal,
    Kobold,
    LightningElemental,
    Minotaur,
    Nightmare,
    Ogre,
    Phantom,
    Quagga,
    Rat,
    Slime,
    TimeElemental,
    Unicorn,
    VampireBat,
    Wraith,
    Xeno,
    YellowMold,
    Zombie
}

public enum Colors
{
    None=0,
    White=1,
    Black=2,
    Red=3,
    Gray=4,
    Green=5,
    Yellow=6,
    Cyan=7,
    Pink=8,
    Blue=9,
    Clear=10,
    Orange=11,
    Purple=12,
    Lime=13,
}

public enum Stats
{
    None=0,
    SP=1,
    MaxHP=2,
    HP=3,
    MaxST=4,
    ST=5,
    DM=6,
    Vision=7,
}

public enum Tags
{
    None=0,
    Terrain=1,
    Creature=2,
    Beefy=3,
    Timeless=4,
    Immaterial=6,
    Projectile=7,
    Lobbed=8,
    Invisible=9,
}

public enum Factions
{
    None = 0,
    Player=1,
    Monster=2,e
}

public static class Prefabs{

    public static void Imprint(Actor who, Things what)
    {
        who.Type = what;
        switch (what)
        {
            case Things.Player:
                who.AddTrait(new AliveTrait());
                who.AddTrait(new PlayerTrait());
                if (!PlayerState.Load(who))
                {
                    who.SetStat(Stats.SP, 6).SetStat(Stats.MaxHP, 10).SetStat(Stats.MaxST, 10).SetStat(Stats.DM, 2);
                    who.AddAction(new DashAction());
                    who.AddAction(new SwingAction());
                }
                who.Imprint('@',Color.white);
//                who.AddAction(new PThrowRock(),2);
                who.Faction = Factions.Player;
                who.Body.gameObject.layer = 10;
                who.Collider.radius *= 0.5f;
                return;
            case Things.Corpse:
                who.Imprint('x',Color.gray);
                who.AddTag(Tags.Terrain);
                return;
            case Things.SlimeTrail:
                who.Imprint('#',Color.gray,Color.green);//Color.Lerp(Color.green,Color.black,0.5f)
                who.AddTag(Tags.Terrain);
                who.SetStat(Stats.MaxHP, 3);
                who.AddAction(new VanishAction(Color.green));
                return;
            case Things.FireTrail:
                who.Imprint('#',Color.gray,Color.red);//Color.Lerp(Color.green,Color.black,0.5f)
                who.AddTag(Tags.Terrain);
                who.AddTag(Tags.Projectile);
                who.SetStat(Stats.MaxHP, 1);
                who.Faction = Factions.Monster;
                who.Payload.Add(new GMsg(GMsgs.TakeDamage).SetAmount(2));
                who.AddAction(new VanishAction(Color.red));
                return;
            case Things.Goblin:
                Enemy(who,5,6);
                who.Imprint('g',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction());
                return;
            case Things.Kobold:
                Enemy(who,3,6);
                who.Imprint('k',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new BombToss("bomb",Colors.Red,2,5,5,new GMsg(GMsgs.TakeDamage).SetAmount(2)));
                return;
            case Things.TimeElemental:
                Enemy(who,5,3);
                who.Imprint('t',Color.green);
                who.AddTag(Tags.Timeless);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1.25f));
                return;
            case Things.Xeno:
                Enemy(who,6,4);
                who.Imprint('x',Color.green);
                who.AddTag(Tags.Timeless);
                who.AddTag(Tags.Immaterial);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1.25f));
                return;
            case Things.Zombie:
                Enemy(who,8,3);
                who.Imprint('z',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1));
                who.AddTag(Tags.Beefy);
                return;
            case Things.Phantom:
                Enemy(who,4,6);
                who.Imprint('@',Color.clear);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1f));
                return;
            case Things.Slime:
                Enemy(who,1,4);
                who.SetStat(Stats.Vision, 3);
                who.Imprint('s',Color.green);
                who.AddAction(new SlimeWanderAction());
                who.AddAction(new SlimeAttackAction(1,720,3,"$'s Slap",0.5f).SetSymbol("**",new Vector2(1,0.25f)));
                return;
            case Things.Archer:
                Enemy(who,2,5);
                who.Imprint('a',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ShootAction());
                return;
            case Things.FlameElemental:
                Enemy(who,6,4,2);
                who.Imprint('f',Color.green);
//                who.AddAction(new BulletHellAction(6));
                who.AddAction(new BulletHellAction(12));
                return;
            case Things.Nightmare:
                Enemy(who,8,8,2);
                who.Imprint('n',Color.green);
//                who.AddAction(new BulletHellAction(6));
                who.AddAction(new BulletHellAction(12,"Fire",Colors.Red,Things.FireTrail));
                return;
            case Things.YellowMold:
                Enemy(who,1,0,2);
                who.Imprint('y',Color.green);
//                who.AddAction(new BulletHellAction(6));
                who.AddAction(new BulletHellAction(0.86f,"Spore",Colors.Yellow));
                return;
            
            case Things.Elf:
                Enemy(who,5,7);
                who.Imprint('e',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ShootAction(0.5f,0.1f));
                return;
            case Things.Beetle:
                Enemy(who,5,5,1);
                who.Imprint('b',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ConeOfFlame(3,3,5,15,"Spray",Colors.Yellow));
                return;
            case Things.IceElemental:
                Enemy(who,6,5,1);
                who.Imprint('i',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ConeOfFlame(3,3,5,15,"Ice",Colors.Cyan,2,new GMsg(GMsgs.TakeAction).SetFAct(new FrozenAction(1))));
                return;
            case Things.Hellhound:
                Enemy(who,7,7,2);
                who.Imprint('h',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ConeOfFlame(4,3,5,15,"Fire",Colors.Red));
                return;
            case Things.Dragon:
                Enemy(who,25,4,3);
                who.Imprint('d',Color.green);
                who.SetSize(2f);
                who.AddAction(new WanderAction());
                who.AddAction(new ConeOfFlame(5,5,10,30,"Dragonfire",Colors.Pink));
                return;
            case Things.Ogre:
                Enemy(who,10,5,4);
                who.Imprint('o',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1,320,3,"$'s Smash").SetSymbol("<&&&&",new Vector2(2.2f,1)));
                who.SetSize(1.5f);
                who.AddTag(Tags.Beefy);
                return;
            case Things.Minotaur:
                Enemy(who,10,5,4);
                who.Imprint('m',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ChargeAction());
                who.AddTag(Tags.Beefy);
                who.SetSize(2f);
                who.AP.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
                return;
            case Things.Rat:
                Enemy(who,2,7,1);
                who.Imprint('r',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(0.5f,0,4,"$'s Bite",0.3f).SetSymbol(">",new Vector2(0.5f,0.5f)));
                return;
            case Things.Cockatrice:
                Enemy(who,3,4,1);
                who.Imprint('c',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(1.5f,0,2,"Petrified By $'s Peck",1f,new GMsg(GMsgs.Petrify)).SetSymbol(">",new Vector2(0.5f,0.5f)));
                return;
            case Things.Jackal:
                Enemy(who,5,7,2);
                who.Imprint('j',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new AttackAction(0.5f,0,4,"$'s Bite",0.3f).SetSymbol(">",new Vector2(0.5f,0.5f)));
                return;
            case Things.Unicorn:
                Enemy(who,9,6,5);
                who.Imprint('u',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ChargeAction().SetSymbol("====>",new Vector2(2.5f,0.5f)));
                return;
            case Things.LightningElemental:
                Enemy(who,3,3,4);
                who.Imprint('l',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new LChargeAction());
                return;
            case Things.Quagga:
                Enemy(who,5,6,2);
                who.Imprint('q',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ChargeAction().SetSymbol(")",new Vector2(0.5f,0.5f)));
                return;
            case Things.VampireBat:
                Enemy(who,2,6,2);
                who.Imprint('v',Color.green);
                who.AddAction(new WanderAction());
                who.AddAction(new ChargeAction(6,3,"$'s Bite").SetSymbol(">",new Vector2(0.5f,0.5f)));
                return;
            case Things.Wraith:
                Enemy(who,6,6,3);
                who.Imprint('w',Color.green);
                who.AddTag(Tags.Immaterial);
                who.AddTag(Tags.Beefy);
                who.AddAction(new WanderAction());
                who.AddAction(new ChargeAction(3,6,"$'s Touch").SetSymbol("<",new Vector2(0.5f,0.5f)));
                return;
            
            case Things.Projectile:
                who.Imprint('*',Color.red);
                who.AddTag(Tags.Projectile);
                who.AddTrait(new ProjectileTrait());
//                who.Hitbox.radius *= 0.5f;
                who.Collider.radius *= 0.1f;
                who.Body.gameObject.layer = 10;
                return;
            case Things.Grass:
                who.Imprint(',',Color.green,Color.Lerp(Color.green,Color.black, 0.5f));
                who.AddTag(Tags.Terrain);
                return;
            case Things.Stairs:
                who.Imprint('>',Color.white,Color.Lerp(Color.blue,Color.black, 0.5f));
                who.AddTag(Tags.Terrain);
                who.Payload.Add(new GMsg(GMsgs.Stairs));
                return;
            case Things.Gear:
                who.Gear = Library.GetRandomGear();
                who.Gear.Imprint(who);
                who.AddTag(Tags.Terrain);
//                who.Imprint('%',Color.cyan);
                who.Payload.Add(new GMsg(GMsgs.GetGear));
                return;
            
            case Things.Amulet:
                who.Name = "Amulet of Yendor";
                who.AddTag(Tags.Terrain);
                who.Imprint('"',Color.white,Color.Lerp(Color.yellow,Color.black, 0.5f));
                who.Payload.Add(new GMsg(GMsgs.WinGame));
                return;
            case Things.Gold:
                int amt = God.Roll(10, PlayerState.Floor * 5);
                who.SetStat(Stats.MaxST, amt);
                who.Name = amt + " Gold Coins";
                who.AddTag(Tags.Terrain);
                who.Imprint('$',Color.yellow);
                who.Payload.Add(new GMsg(GMsgs.GetCoins));
                return;
        }
    }

    public static void Enemy(Actor who, float hp=2, float speed=6,float dmg=2)
    {
        who.Body.gameObject.layer = 8;
        who.AddTrait(new AliveTrait());
        who.SetStat(Stats.SP,speed).SetStat(Stats.MaxHP,hp).SetStat(Stats.DM,dmg);
        who.Faction = Factions.Monster;
    }
}