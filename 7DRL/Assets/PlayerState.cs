﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerState
{
    public static bool Setup = false;
    public static List<ActTracker> KnownActions;
    public static Dictionary<Stats, float> MyStats;
    public static int Floor = 1;
    public static float Score = 0;

    public static void Save(Actor who)
    {
        if (who == null)
            return;
        Setup = true;
        KnownActions = who.KnownActions;
        MyStats = who.MyStats;
    }
    
    public static bool Load(Actor who)
    {
        if (!Setup)
            return false;
        who.KnownActions = KnownActions;
        who.MyStats = MyStats;
        return true;
    }

    public static void Wipe()
    {
        KnownActions = new List<ActTracker>();
        MyStats = new Dictionary<Stats, float>();
        Score = 0;
        Floor = 1;
        Setup = false;
    }
}
