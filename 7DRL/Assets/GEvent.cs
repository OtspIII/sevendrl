﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MTags
{
    None=0,
    LowKnockback=1,
}

public class GMsg
{
    public GMsgs Type;
    public Actor Target;
    public Actor Source;
    public CharAction Act;
    public float Amount;
    public CharAction ForceAct;
    public List<MTags> Tags = new List<MTags>();
    public Things Thing;
    
    public GMsg()
    {
		
    }
    public GMsg(GMsgs type,Actor source=null)
    {
        Type = type;
        Source = source;
    }
   
    public GMsg(GMsg clone,Actor targ=null)
    {
        Clone(clone);
        if (targ != null)
            Target = targ;
    }

    GMsg Clone(GMsg clone)
    {
        Type = clone.Type;
        Target = clone.Target;
        Source = clone.Source;
        Act = clone.Act;
        Amount = clone.Amount;
        ForceAct = clone.ForceAct;
        Tags.AddRange(clone.Tags);
        Thing = clone.Thing;
        return this;
    }

    public void Send(Actor target = null, Actor source = null, CharAction act = null)
    {
        if (target != null)
            Target = target;
        if (source != null)
            Source = source;
        if (act != null)
            Act = act;
//        Debug.Log("SEND: " + this + " / " + Type);
        God.GM.Messages.Add(this);
    }

    public GMsg SetAmount(float a)
    {
        Amount = a;
        return this;
    }
    
    public GMsg SetThing(Things t)
    {
        Thing = t;
        return this;
    }

    public GMsg SetFAct(CharAction a)
    {
        ForceAct = a;
        return this;
    }

    public GMsg AddTags(params MTags[] t)
    {
        foreach(MTags tag in t)
            if (!Tags.Contains(tag))
                Tags.Add(tag);
        return this;
    }


}

public enum GMsgs
{
    None,
    TakeDamage,
    Inputs,
    TakeAction,
    Stairs,
    GetGear,
    WinGame,
    Die,
    GetCoins,
    Petrify,
    Teleport,
    Heal,
    Invisible,
    Anger,
    Reflect,
    Polymorph,
    Summon,
}
