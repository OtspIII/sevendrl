﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionThing : MonoBehaviour
{
    public SpriteRenderer Img;
    public CircleCollider2D Col;
    public float Timer;
    public float MaxTimer = 0.3f;
    public Color StartC;

    public void Imprint(float size,Colors c)
    {
        StartC = God.GetColor(c);
        transform.localScale = new Vector3(size,size,size);
        Img.color = StartC;
        Timer = 0;
    }

    private void Update()
    {
        Timer += God.GetTime(true);
        Img.color = Color.Lerp(StartC, Color.black, Timer / MaxTimer);
        if (Timer > MaxTimer)
            Destroy(gameObject);
    }
}
