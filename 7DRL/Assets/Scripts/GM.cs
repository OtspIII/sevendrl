﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
//using UnityEngine.XR.WSA.Persistence;
using Random = UnityEngine.Random;

public class GM : MonoBehaviour
{
    public GameObject RBHolder;
    public List<GMsg> Messages = new List<GMsg>();
    public Actor Player;
    public SpriteRenderer Fade;
    public GameMode Mode;
    public ActTracker PlayerAct;
    public Transform BotBar;
    public ActionCard ACPrefab;
    public bool JustClicked;
    public SpriteRenderer Cursor;
    public SpriteRenderer Threat;
    public Actor Prefab;
    public bool BotBarQueue = true;
//    public Things[] ToSpawn;
    public float Tiredness = 0;
    public TextMeshPro Status;
    public Camera Cam;
    public Dungeon D;
    public WallThing WallP;
    public Transform Map;
    public MessageBox MB;
    public Headtext HTP;
    public ExplosionThing ExplodeP;
    public List<Actor> Actors;
    

    public void Awake()
    {
        God.GM = this;
        IM.Init();
        Library.Init();
        SetMode(GameMode.Default);
    }

    private void Start()
    {
        D = new Dungeon();
        D.Build(PlayerState.Floor);
        if (PlayerState.Floor == 1)
            ShowMessage("Welcome","Can you reach the dungeon's bottom and claim the Amulet of Yendor?\n\n<u>Controls</u>\nWASD - Move\nSpace - Toggle Time\nShift - Half Speed\n1-6 - Select Action\nLeft Click - Perform Action");
        else
            SetMode(GameMode.ChooseAction);
//        Debug.Log("X: " + ));
        Vector3 rbHold = RBHolder.transform.position;
        rbHold.x = Cam.ViewportToWorldPoint(new Vector3(1, 0.5f)).x;
        RBHolder.transform.position = rbHold;
//        Spawn(Things.Player, new Vector2(8, -1));
//        foreach (Things t in ToSpawn)
//        {
//            Spawn(t, new Vector2(Random.Range(-11, 0), Random.Range(-6, 6)));
//        }
    }

    public void ShowMessage(string title, string desc,bool endGame=false,float spd=0.6f)
    {
        MB.Imprint(title,desc);
        StartCoroutine(MB.Flyby(spd,endGame));
    }

    public void SetPlayer(Actor p)
    {
        Player = p;
        BotBarQueue = true;
    }

    void Update()
    {
//        if (IM.Click())
//        {
//            List<Actor> hit = SpawnExplosion(God.MouseLoc(), Factions.None, 2);
//            foreach (Actor a in hit)
//            {
//                new GMsg(GMsgs.TakeDamage).SetAmount(1).Send(a);
//            }
//        }
        if (IM.GetButton(Inputs.RBRIght))
            RBHolder.transform.position += new Vector3(0.01f,0,0);
        if (IM.GetButton(Inputs.RBLeft))
            RBHolder.transform.position += new Vector3(-0.01f,0,0);
        if (Player != null)
        {
//            Debug.Log("A");
            Player.TakeMsg(new GMsg(GMsgs.Inputs));
            Tile where = Player.GetTile();
            if (where?.Contents != null && where.Contents.Type == Things.SlimeTrail && Mode != GameMode.LoadScene)
                SetMode(GameMode.Slimed);
            else if (Mode == GameMode.Slimed)
                SetMode(GameMode.Default);
            if (Mode == GameMode.Slimed)
            {
                if (PlayerAct != null)
                {
                    Debug.Log("PREVIEW: " + Time.time);
                    PlayerAct.Act.Preview(Player);
                }
                if (IM.Click() && !JustClicked)
                {
                    if (PlayerAct != null)
                        Player.SetAct(PlayerAct);
                }
            }
            else if (Mode == GameMode.ChooseAction)
            {
                if (IM.GetButton(Inputs.SpeedUp))
                    SetTime(Mathf.Lerp(God.TimeDefault, GameSettings.FFSpeed, 0.1f));
                else
                    SetTime(GameSettings.ChooseSpeed);
                if (PlayerAct != null)
                {
                    PlayerAct.Act.Preview(Player);
                }
                if (IM.Click() && !JustClicked)
                {
                    if (PlayerAct != null)
                        Player.SetAct(PlayerAct);
                }
                else if (IM.GetButtonDown(Inputs.Escape))
                {
                    PlayerAct = null;
                    SetMode(GameMode.Default);
                }
            }
            CameraFollow();
        }
        Status.text = GetStatus();
        if (BotBarQueue)
        {
            SetupBotBar();
            BotBarQueue = false;
        }
        JustClicked = false;
    }
    
    void FixedUpdate()
    {
        while(Messages.Count > 0)
        {
            GMsg m = Messages[0];
            Messages.Remove(m);
//            Debug.Log("TM: " + m.Target + " / " + m.Type);
            m.Target.TakeMsg(m);
        }
    }

    public enum GameMode
    {
        Default=0,
        ChooseAction=1,
        ResolveAction=2,
        LoadScene=3,
        Slimed=4,
    }

    public string GetStatus()
    {
        string r = "";
        if (Player == null)
            return r;
        r += "FLOOR " + PlayerState.Floor + "\n";
        r += "SCORE " + (int)PlayerState.Score + "\n";
        r += "HP:" + Player.GetStat(Stats.HP) + "/" + Player.GetStat(Stats.MaxHP) + "\n";
        r += "ST:" + Player.GetStat(Stats.ST) + "/" + Player.GetStat(Stats.MaxST) + "\n";
        r += "SP:" + Player.GetStat(Stats.SP) + "  DM:" + Player.GetStat(Stats.DM);
        return r;
    }

    public void SetMode(GameMode m)
    {
        if (m == Mode)
            return;
//        Debug.Log("SET MODE: " + m);
        Mode = m;
//        WipeBotBar();
        BotBarQueue = true;
        switch (Mode)
        {
            case GameMode.ChooseAction:
                SetFade(0.2f);
                SetTime(GameSettings.ChooseSpeed);
//                SetupBotBar();
                break;
            case GameMode.Default:
                SetFade(0f);
                SetTime(1f);
                PlayerAct = null;
                break;
            case GameMode.Slimed:
                SpawnHeadtext(Player,"slimed",Colors.Green);
                SetFade(0.3f,Colors.Green);
                SetTime(1f);
                break;
            case GameMode.ResolveAction:
                SetFade(0.1f);
                float speed = PlayerAct != null && PlayerAct.Act.ResolveSpeed != -1 ?
                    PlayerAct.Act.ResolveSpeed : GameSettings.ActSpeed; 
                SetTime(speed);
                break;
            case GameMode.LoadScene:
                SetFade (0);
                SetTime(0);
                break;
        }
    }

    public void WipeBotBar()
    {
        Cursor.gameObject.SetActive(false);
        while (BotBar.transform.childCount > 0)
        {
            GameObject g = BotBar.transform.GetChild(0).gameObject;
            g.transform.parent = null;
            Destroy(g);
        }
    }
    
    public void SetupBotBar()
    {
        WipeBotBar();
        if (Player == null)
            return;
        Vector2 pos = Vector2.zero;
        int n = 0;
        foreach (ActTracker a in Player.KnownActions)
        {
            ActionCard ac = Instantiate(ACPrefab.gameObject).GetComponent<ActionCard>();
            ac.Imprint(a,n);
            ac.transform.parent = BotBar;
            ac.transform.localPosition = pos;
            pos.y -= 2;
            n++;
        }
    }

    public void SetFade(float a, Colors c = Colors.White)
    {
        if (a == 0)
        {
            Fade.gameObject.SetActive(false);
            return;
        }
        Fade.gameObject.SetActive(true);
        Color col = God.GetColor(c);
        col.a = a;
        Fade.color = col;
    }

    public void SetCursor(Vector2 where,float threat=0)
    {
        Cursor.gameObject.SetActive(true);
        Cursor.transform.position = where;
        if (threat <= 0)
        {
            Threat.gameObject.SetActive(false);
            return;
        }
        Threat.gameObject.SetActive(true);
        float size = threat / 15;
        Threat.transform.localScale = new Vector3(size,size,size);
    }

    public Actor SpawnProjectile(string name,Colors c, Actor parent, CharAction act, Vector2 target, float speed = 30, params GMsg[] payload)
    {
        return SpawnProjectile(name,c,parent,act,parent.transform.position,target,parent.Faction,speed,payload);
    }

    public Actor SpawnProjectile(string name,Colors c,Actor parent, CharAction act,Vector2 where,Vector2 target,Factions f = Factions.None, float speed=30,params GMsg[] payload)
    {
        Vector2 temp = new Vector2(Random.Range(9999,999999),Random.Range(9999,999999));
        where = Vector2.MoveTowards(where, target, 0.2f);
        Actor r = Instantiate(Prefab.gameObject,temp,Quaternion.identity).GetComponent<Actor>();
        r.Setup(Things.Projectile);
        r.Name = name;
        r.Payload.AddRange(payload);
        r.WalkMove = (target - where).normalized * speed;
        r.Faction = f;
        r.Parent = parent;
        r.ParentAct = act;
        r.Body.gameObject.layer = 11;
        r.transform.position = where;
        r.Body.color = God.GetColor(c);
        if (name == "potion" && parent == Player)
            r.AddTag(Tags.Timeless);
        return r;
    }
    
    public Actor SpawnLob(string name,Colors c,Actor parent, CharAction act,Vector2 where,Vector2 target,Factions f = Factions.None, float speed=30,float size=2,params GMsg[] payload)
    {
        Vector2 temp = new Vector2(Random.Range(9999,999999),Random.Range(9999,999999));
        where = Vector2.MoveTowards(where, target, 0.2f);
        Actor r = Instantiate(Prefab.gameObject,temp,Quaternion.identity).GetComponent<Actor>();
        r.Setup(Things.Projectile);
        r.Name = name;
        r.AddTag(Tags.Lobbed);
        r.Payload.AddRange(payload);
        r.WalkMove = (target - where).normalized * speed;
        r.Faction = f;
        r.Parent = parent;
        r.ParentAct = act;
        r.Body.gameObject.layer = 11;
        r.transform.position = where;
        r.Body.color = God.GetColor(c);
        r.SetAct(new BeLobbedAction(target,size));
        if (name == "potion" && parent == Player)
            r.AddTag(Tags.Timeless);
        return r;
    }

    public Actor Spawn(Things what, Tile where)
    {
        if (where == null)
        {
            Debug.Log("NULL WHERE: " + what);
            return null;
        }
        Point p = where.Loc;
        if (where.R != null)
            where.R.EmptyTiles.Remove(where);
        D.EmptyTiles.Remove(where);
        Actor r = Spawn(what, p.ToVector());
        if (r.HasTag(Tags.Terrain))
            where.Contents = r;
        return r;
    }
    
    public Actor Spawn(Things what, Vector2 where)
    {
        Actor r = Instantiate(Prefab.gameObject,where,Quaternion.identity).GetComponent<Actor>();
        r.Setup(what);
        return r;
    }

    public void UpdateStamina()
    {
        if (Player == null || Player.GetStat(Stats.MaxST) <= 0)
            return;
        Tiredness = 1 - (Player.GetStat(Stats.ST) / Player.GetStat(Stats.MaxST));
        SetTime();
//        Debug.Log("SET TIRED: " + Tiredness);
    }

    public void SetTime(float t=-1)
    {
        if (t == -1)
            t = God.TimeDefault;
        God.TimeDefault = t;
//        t = Mathf.Lerp(t, 1, Tiredness*Tiredness);
        God.TimeScale = t;
//        Debug.Log("SET TIME: " + God.TimeScale + " / " + God.TimeDefault);
    }

    public void CameraFollow()
    {
        if (Player == null)
            return;
        Vector3 loc = Player.transform.position;
        Vector3 cam = Cam.transform.position;
        if (cam.x < loc.x - 2)
            cam.x = loc.x - 2;
        else if (cam.x > loc.x + 3)
            cam.x = loc.x + 3;
        if (cam.y < loc.y - 2)
            cam.y = loc.y - 2;
        else if (cam.y > loc.y + 2)
            cam.y = loc.y + 2;
        Cam.transform.position = cam;
    }

    public void SpawnWall(RectInt shape)
    {
//        Debug.Log("SPAWN WALL: " + shape);
        WallThing wt = Instantiate(WallP).GetComponent<WallThing>();
        wt.Build(shape);
        wt.transform.SetParent(Map);
    }

    public void LoadScene(string scene)
    {
        SetMode(GameMode.LoadScene);
        PlayerState.Save(Player);
        StartCoroutine(loadScene(scene));
    }

    public IEnumerator loadScene(string scene,float dur=1)
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / dur;
            SetFade(t);
            yield return null;
        }
        SetFade(1);
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }

    public void SpawnHeadtext(Actor who, string txt, Colors c)
    {
        Headtext ht = Instantiate(HTP.gameObject, who.transform.position + new Vector3(0, 0.5f * who.transform.localScale.y, 0),
                Quaternion.identity).GetComponent<Headtext>();
        ht.Imprint(txt,c);
    }
    
    public List<Actor> SpawnExplosion(Vector2 where,Factions f = Factions.None,float size=2)
    {
        Vector3 wh = where;
        wh.z = 20;
        ExplosionThing exp = Instantiate(ExplodeP, wh, Quaternion.identity).GetComponent<ExplosionThing>();
        exp.Imprint(size,Colors.Red);
        List<Actor> r = new List<Actor>();
        foreach (Actor a in Actors)
        {
            if (Vector2.Distance(a.transform.position,where) < size/2+0.1f)
                r.Add(a);
        }
//        List<Collider2D> res = new List<Collider2D>();
//        ContactFilter2D fil = new ContactFilter2D();
//        fil.layerMask = ~LayerMask.GetMask("Default");
//        exp.Col.OverlapCollider(fil, res);
//        foreach (Collider2D c in  res)
//        {
//            Debug.Log("EXP HITS: " + c);
//            Actor a = c.GetComponentInParent<Actor>();
//            if (a != null && !r.Contains(a))
//                r.Add(a);
//        }
        return r;
    }
}
