﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WorldThing : Thing
{
    public TextMeshPro Body;
    public Collider2D C
    {
        get
        {
            if (c == null) c = GetComponent<Collider2D>();
            return c;
        }
    }
    private Collider2D c;
    public Color DefaultColor;
    

    public override void OnReset()
    {
        GameObject bod = null;
        for (int n = 0; n < transform.childCount; n++)
        {
            if (transform.GetChild(n).name == "Body")
            {
                bod = transform.GetChild(n).gameObject;
                break;
            }
        }

        if (bod != null)
        {
            Body = bod.GetComponent<TextMeshPro>();
//            bod = new GameObject("Body");
//            bod.transform.parent = transform;
//            bod.transform.localScale = Vector3.zero;
//            bod.AddComponent()
        }
    }
    
    

}
