﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using TMPro;
//using UnityEditor.IMGUI.Controls;
using UnityEngine;
//using UnityEngine.Serialization;
using Random = System.Random;

public class Actor : WorldThing
{
    public string Name;
    public Vector2 WalkMove;
    public bool canAct = true;
//    public float Speed;
    public Rigidbody2D RB;
    public List<Trait> TraitList = new List<Trait>();
    public Dictionary<GMsgs,List<Trait>> Listeners = new Dictionary<GMsgs, List<Trait>>();
    public ActTracker Action;
    public Things Type;
    public List<ActTracker> KnownActions = new List<ActTracker>();
    public ActPivot AP;
    public Dictionary<Stats, float> MyStats = new Dictionary<Stats, float>();
    public Actor Target;
    public List<GMsg> Payload = new List<GMsg>();
    public Factions Faction;
    public CircleCollider2D Hitbox;
    public CircleCollider2D Collider;
    public bool IsSetup = false;
    public SpriteRenderer BG;
    public List<Tags> TagList = new List<Tags>();
    public GameObject TarPivot;
    public GameObject TarTip;
    public TextMeshPro Desc;
    public Actor Parent;
    public CharAction ParentAct;
    public GearEntry Gear;
    public Transform APTip;

    public float DescPower = 0;
//    public bool Pickup = false;

    public override void OnStart()
    {
        if (Type == Things.None)
            return;
        Setup(Type);
        AP.Rotate(UnityEngine.Random.Range(0,360));
        DescPower = -3;
        God.GM.Actors.Add(this);
    }

    public float GetEnergy()
    {
        float r = 1;
        if (GetStat(Stats.MaxST) == 0)
            return r;
        return Mathf.Max(GetStat(Stats.ST) / GetStat(Stats.MaxST),0.01f);
    }

    public Actor AddAction(CharAction act, int uses = -1)
    {
        if (uses == -1)
            uses = act.MaxUses;
        KnownActions.Add(new ActTracker(act,uses));
        act.MaxUses = uses;
        God.GM.BotBarQueue = true;
        return this;
    }

    public void Setup(Things type)
    {
        if (IsSetup)
            return;
        IsSetup = true;
        Type = type;
        Name = God.Niceify(type.ToString()).ToLower();
        Prefabs.Imprint(this,Type);
        Action = FindDefaultState();
        if (MyStats.ContainsKey(Stats.MaxHP) && GetStat(Stats.HP) == 0)
            SetStat(Stats.HP, GetStat(Stats.MaxHP));
        if (MyStats.ContainsKey(Stats.MaxST) && GetStat(Stats.ST) == 0)
            SetStat(Stats.ST, GetStat(Stats.MaxST));
        SetLoc(transform.position);
    }

    public void SetLoc(Vector3 loc)
    {
        loc.z = 0;
        if (HasTag(Tags.Terrain))
            loc.z = 10;
        transform.position = loc;
    }

    public void Imprint(char icon, Color c)
    {
        Imprint(icon,c,Color.clear);
    }
    
    public void Imprint(char icon, Color c, Color bg)
    {
        Body.text = icon.ToString();
        Body.color = c;
        DefaultColor = c;
        if (bg.a == 0)
            BG.gameObject.SetActive(false);
        else
        {
            BG.gameObject.SetActive(true);
            BG.color = bg;
        }

        switch (icon)
        {
            case ',' : Body.margin = new Vector4(0,-0.5f,0,0); break;
//            case '*': Body.margin = new Vector4(0,-0.1f,0,0); break;
        }
    }

    public List<CharAction> GetActions()
    {
        List<CharAction> r = new List<CharAction>();
        foreach(ActTracker a in KnownActions)
            r.Add(a.Act);
        return r;
    }

    public void SetColor(Colors c,float lerp=1)
    {
        if (c == Colors.None)
            Body.color = DefaultColor;
        else if (lerp == 1)
            Body.color = God.GetColor(c);
        else
            Body.color = Color.Lerp(DefaultColor, God.GetColor(c),lerp);
    }

    public void AddTrait(Trait t)
    {
        TraitList.Add(t);
        foreach (GMsgs m in t.ListenFor)
        {
            if (!Listeners.ContainsKey(m))
                Listeners.Add(m,new List<Trait>());
            Listeners[m].Add(t);
        }
        t.Setup(this);
    }
    
    public virtual bool CanAct()
    {
        if (Action != null && !Action.Act.AllowMove)
            return false;
        return canAct;
    }

    public float GetStat(Stats s)
    {
        return MyStats.ContainsKey(s) ? MyStats[s] : 0;
    }

    public Actor SetStat(Stats s, float v)
    {
        if (MyStats.ContainsKey(s))
            MyStats[s] = v;
        else
            MyStats.Add(s,v);
//        Debug.Log("A: " + v + " / " + s);
        if (s == Stats.ST && God.GM.Player == this)
        {
//            Debug.Log("X: " + v);
            God.GM.UpdateStamina();
        }
        return this;
    }

    public Vector2 APDir()
    {
        return (AP.transform.position - transform.position).normalized;
    }
    
    public Actor ChangeStat(Stats s, float v)
    {
        float o = GetStat(s);
        o += v;
        if (s == Stats.ST)
            o = Mathf.Clamp(o, 0,GetStat(Stats.MaxST));
        else if (s == Stats.HP)
            o = Mathf.Clamp(o, 0,GetStat(Stats.MaxHP));
        SetStat(s, o);
//        if (MyStats.ContainsKey(s))
//            MyStats[s] += v;
//        else
//            MyStats.Add(s,v);
        return this;
    }

    public void SetAct(CharAction ca)
    {
        if (ca == null)
            SetAct(null,null,false);
        else
            SetAct(new ActTracker(ca,999),null,true);
    }

    public void SetAct(ActTracker ca, Actor targ=null,bool forced=false)
    {
//        if (Type==Things.Player) Debug.Log("SET A: " + this + " / " + ca?.Act);
        AP.gameObject.SetActive(false);
        if (Action != null)
        {
            Action.Act.End(this,forced);
            if (Action == God.GM.PlayerAct)
            {
                God.GM.SetMode(GM.GameMode.ChooseAction);
            }
        }
        bool def = ca == null;
        
        if (def)
            ca = FindDefaultState();
        Action = ca;
        Action.Act.Start(this,targ);

        if (KnownActions.Contains(Action) && Action.Uses != -1)
        {
            Action.Uses--;
            if (Action.Uses <= 0)
            {
                Action.UsedUp(this);
                KnownActions.Remove(Action);
//                if (God.GM.PlayerAct == Action)
//                    God.GM.PlayerAct = null;
            }
            God.GM.BotBarQueue = true;
        }

        if (Action.Act.StaminaCost == 0 && !(Action.Act is PotionToss || Action.Act is RodAction))
            def = true;
        
        if (!def && God.GM.Player == this && ca.Act.PlayerAction && (God.GM.Mode == GM.GameMode.ChooseAction || God.GM.Mode == GM.GameMode.Slimed))
        {
//            Debug.Log("SET B: " + this + " / " + ca?.Act);
            God.GM.PlayerAct = ca;
            God.GM.SetMode(GM.GameMode.ResolveAction);
        }
        else if (def && God.GM.Player == this)
            God.GM.SetMode(GM.GameMode.ChooseAction);
        
    }
    
    

    public ActTracker FindDefaultState()
    {
        if (God.GM.Player == this)
            return new ActTracker(new IdleAction(),999);
        if (KnownActions.Count > 0)
            return KnownActions[0];
        return new ActTracker(new IdleAction(),999);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        WorldThing o = other.gameObject.GetComponent<WorldThing>();
        if (Action == null || o == null || o is Actor)
            return;
        Action.Act.OnBump(this, o);
        if (HasTag(Tags.Projectile) && o != Parent)
        {
//            Debug.Log("PROJ HIT: " + o + " / " + Time.time);
            Destruct();
        }
    }

    public void SetSize(float s)
    {
        transform.localScale = new Vector3(s,s,s);
    }

    public bool ValidTarget(Actor who)
    {
        if (HasTag(Tags.Lobbed))
            return false;
        if (who == null || who == this || who == Parent || Action == null)
            return false;
        if (Faction != Factions.None && who.Faction == Faction)
            return false;
        return who.HasTag(Tags.Creature);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
//        Debug.Log("OCE: " + this + " / " + other);
        Actor o = other.gameObject.GetComponent<Actor>();
        if (ValidTarget(o))
        {
//            Debug.Log("B: " + this + " / " + o + " / " + o.Faction + " / " + Faction + " / " + Payload.Count);
            Action.Act.OnTouch(this, o);
//            if (!Pickup || o == God.GM.Player)
                foreach (GMsg m in Payload)
                {
                    GMsg e = new GMsg(m);
                    Actor src = Parent != null ? Parent : this;
                    e.Send(o,src,ParentAct != null ? ParentAct : Action.Act);
                }

                if (Type == Things.Projectile)
                {
//                    Debug.Log("PROJ HIT: " + o + " / " + Faction + " / " + o.Faction + " / " + Parent + " / " + (o == Parent) + " / " + Time.time);
                    Destruct();
                }

                return;
        }

        ActPivot ap = other.gameObject.GetComponentInParent<ActPivot>();
        if (ap?.Who?.Action != null && !ap.Who.Action.Act.Hit.Contains(this))
        {
            ap.Who.Action.Act.Hit.Add(this);
//            Debug.Log("OTE AP: " + other + " / " + ap + " / " + ap.Who.State);
            ap.Who.Action.Act.OnHit(ap.Who, this);
        }

    }

    private void FixedUpdate()
    {
        if (HasTag(Tags.Timeless))
            RB.velocity = WalkMove;
        else
            RB.velocity = WalkMove * God.TimeScale;
        Action?.Act.Run(this);
    }

    public override void OnReset()
    {
        base.OnReset();
        RB = GetComponent<Rigidbody2D>();
    }

    public GMsg TakeMsg(GMsg e)
    {
        //Gotta make a new version so I don't mess up the original due to pass by ref
        e = new GMsg(e,this);
        
//        if (e.Type != GMsgs.Inputs)Debug.Log("A: " + e.Type + " / " + this);
        if (Listeners == null || !Listeners.ContainsKey(e.Type))
            return e;
//        if (e.Type != GMsgs.Inputs)Debug.Log("B");
        foreach (Trait t in Listeners[e.Type])
        {
            t.GetMsg(this,e);
        }
        return e;
    }

    public Tile GetTile()
    {
        return God.GM.D.GetTile(Mathf.FloorToInt(transform.position.x+0.5f),Mathf.FloorToInt(transform.position.y+0.5f));
    }

    public string GetName(bool upper=false)
    {
        if (Gear != null)
            return Gear.GetName();
//        string r = Name;
        return upper ? char.ToUpper(Name[0]) + Name.Substring(1) : Name;
    }
    
    private void OnMouseOver()
    {
        DescPower += Time.deltaTime / 0.1f;
        int chars = Mathf.CeilToInt(DescPower);
        string name = GetName();
        string desc = "";
        for (int n = 0; n < chars; n++)
        {
            if (name.Length <= n)
                break;
            if (n == 0 && name[0] == Body.text[0])
                continue;
            desc += name[n];
        }
        Desc.text = desc;
    }

    private void OnMouseExit()
    {
        DescPower = -3;
        Desc.text = "";
    }

    public float VisionRange()
    {
        float r = GetStat(Stats.Vision);
        if (r == 0)
            r = 10;
        return r;
    }

    public bool CanSee(Actor who)
    {
        if (who == null || who.HasTag(Tags.Invisible))
            return false;
        float dist = Vector2.Distance(who.transform.position, transform.position);
        if (dist > VisionRange())
            return false;
        if (HasTag(Tags.Immaterial))
            return true;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, who.transform.position - transform.position,dist,LayerMask.GetMask("Wall"));
        if (hit.collider != null)
        {
//            Actor see = hit.collider.GetComponentInParent<Actor>();
//            Debug.Log("I SEE WALL: "  + " / " + this + " / " + who);
            return false;
        }
        return true;
    }

    public ActTracker ChooseAttack(Actor target)
    {
        if (KnownActions.Count >= 2)
        {
            Target = target;
            return KnownActions[1];
        }

        return null;
    }

    public override string ToString()
    {
        if (Type != Things.None)
            return God.Niceify(Type.ToString()) + " (" + Action + ")";
        return base.ToString();
    }

    public override void Destruct()
    {
        if (God.GM.Player == this)
            God.GM.Player = null;
        if (GetTile() != null && GetTile().Contents == this)
            GetTile().Contents = null;
        base.Destruct();
        Payload.Clear();
        God.GM.Actors.Remove(this);
    }

    public bool HasTag(Tags t)
    {
        return TagList.Contains(t);
    }

    public Actor AddTag(Tags t)
    {
        if (!TagList.Contains(t))
            TagList.Add(t);
        if (t == Tags.Terrain)
        {
            RB.isKinematic = true;
        }
        if (t == Tags.Immaterial)
        {
            Hitbox.gameObject.layer = 13;
        }
        return this;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (!HasTag(Tags.Creature) || this == God.GM.Player)
            return;
        Actor o = other.GetComponent<Actor>();
        if (o == null || !o.HasTag(Tags.Creature) || o == God.GM.Player)
            return;
        Vector2 dir = (o.transform.position - transform.position).normalized;
        o.Push(dir,3);
//        Debug.Log("PUSH: " + this + " / " + o);
    }

    public void Push(Vector2 dir, float amt)
    {
        dir *= amt * God.GetTime();
        Vector2 pos = transform.position;
        RB.MovePosition(pos + dir);
    }
}
