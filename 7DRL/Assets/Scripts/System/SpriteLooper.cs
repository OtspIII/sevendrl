﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLooper : MonoBehaviour {

	public SpriteRenderer SR;
	public List<Sprite> Sprites;
	public float Rate;
	public bool Frames;
	Thing Parent;
	bool HasParent;
	float Timer = 0;
	int Index;

	void Start(){
		Transform t = transform;
		while (t != null && Parent == null) {
			t = t.parent;
			Parent = t.GetComponent<Thing> ();
		}
		if (Parent != null)
			HasParent = true;
	}

	void Update(){
		if (HasParent && Parent == null) {
			Destroy (this);
			return;
		}
		Timer += Frames ? 1 : Time.deltaTime;
		if (Timer >= Rate) {
			Index++;
			if (Index >= Sprites.Count)
				Index = 0;
			SR.sprite = Sprites [Index];
		}
	}

	void Reset(){
		SR = GetComponent<SpriteRenderer> ();
		if (SR != null)
			Sprites.Add (SR.sprite);
		Rate = 1;
		Frames = true;
	}
}
