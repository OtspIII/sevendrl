﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SettingsMaster
{

	public enum Bools
	{
		None,
	}

	public enum Ints
	{
		None,
	}
	
	public enum Floats
	{
		None,
		MusicVolume,
		SFXVolume,
	}

	public enum Strings
	{
		None,
	}
	
	static bool Initialized = false;
	public static Dictionary<Bools,bool> BoolDict = new Dictionary<Bools, bool>();
	public static Dictionary<Ints,int> IntDict = new Dictionary<Ints, int>();
	public static Dictionary<Floats,float> FloatDict = new Dictionary<Floats, float>();
	public static Dictionary<Strings,string> StringDict = new Dictionary<Strings, string>();

	public static void Initialize()
	{
		if (Initialized)
			return;
		Initialized = true;
	}

	public static void Reset(){
		BoolDict.Clear ();
		IntDict.Clear ();
	}

	public static bool GetDefault(Bools v)
	{
		return false;
	}
	public static int GetDefault(Ints v)
	{
		switch (v)
		{
		default: return 0;
		}
	}
	public static float GetDefault(Floats v)
	{
		switch (v)
		{
			case Floats.MusicVolume:
				return 100;
			case Floats.SFXVolume:
				return 100;
		}
		return 0;
	}

	public static string GetDefault(Strings v)
	{
		switch (v)
		{

		}

		return "";
	}

	public static bool GetBool(Bools v)
	{
		if (!BoolDict.ContainsKey (v))
		{
			if (PlayerPrefs.HasKey (v.ToString ())) {
				BoolDict.Add (v, PlayerPrefs.GetInt (v.ToString ()) != 0);
			}
			else
				BoolDict.Add(v,GetDefault(v));
		}
		return BoolDict [v];
	}

	public static void SetBool(Bools v, bool b)
	{
		if (!BoolDict.ContainsKey(v))
			BoolDict.Add(v,b);
		else
			BoolDict [v] = b;
		int n = b ? 1 : 0;
		PlayerPrefs.SetInt (v.ToString (), n);
	}

	public static int GetInt(Ints v)
	{
		if (!IntDict.ContainsKey (v))
		{
			if (PlayerPrefs.HasKey(v.ToString()))
				IntDict.Add (v, PlayerPrefs.GetInt (v.ToString ()));
			else
				IntDict.Add(v,GetDefault(v));
		}
		return IntDict [v];
	}

	public static void SetInt(Ints v, int i)
	{
		if (!IntDict.ContainsKey(v))
			IntDict.Add(v,i);
		else
			IntDict [v] = i;
		PlayerPrefs.SetInt (v.ToString (), i);
	}
	
	public static float GetFloat(Floats v)
	{
		if (!FloatDict.ContainsKey (v))
		{
			if (PlayerPrefs.HasKey(v.ToString()))
				FloatDict.Add (v, PlayerPrefs.GetFloat (v.ToString ()));
			else
				FloatDict.Add(v,GetDefault(v));
		}
		return FloatDict [v];
	}

	public static void SetFloat(Floats v, float i)
	{
		if (!FloatDict.ContainsKey(v))
			FloatDict.Add(v,i);
		else
			FloatDict [v] = i;
		PlayerPrefs.SetFloat (v.ToString (), i);
	}
	
	public static string GetString(Strings v)
	{
		if (!StringDict.ContainsKey (v))
		{
			if (PlayerPrefs.HasKey(v.ToString()))
				StringDict.Add (v, PlayerPrefs.GetString (v.ToString ()));
			else
				StringDict.Add(v,GetDefault(v));
		}
		return StringDict [v];
	}

	public static void SetString(Strings v, string i)
	{
//		Debug.Log("SETSTR: " + v + " / " + i);
		if (!StringDict.ContainsKey(v))
			StringDict.Add(v,i);
		else
			StringDict [v] = i;
		PlayerPrefs.SetString (v.ToString (), i);
	}
	
}

