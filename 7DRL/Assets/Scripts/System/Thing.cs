﻿using UnityEngine;
using System.Collections;

//The base class, from which pretty much everything else inherets.
//Not expecting this to get used a ton. Mostly just a layer between everything and MonoBehavior.
using System.Collections.Generic;


public class Thing : MonoBehaviour 
{
//	public string Name { get { return C == null ? name : C.Name; } }
//	public Concept C;
	public float ZLayerMod { get; protected set; }

	public void Awake()
	{
		OnAwake ();
	}

	public virtual void OnAwake()
	{
		ZLayerMod = Random.Range (0.01f, 9f);
//		transform.position = new Vector3 (9999 + Random.Range (0f, 999f), 9999 + Random.Range (0f, 999f), -999);
	}

	public void Start()
	{
		OnStart ();
	}

	public virtual void OnStart()
	{

	}

	public void Update()
	{
		OnUpdate ();
	}

	public virtual void OnUpdate()
	{
		
	}

//	public void LateUpdate()
//	{
//		OnLateUpdate ();
//	}
//
//	public virtual void OnLateUpdate()
//	{
//
//	}

//	public void FixedUpdate()
//	{
//		OnFixedUpdate ();
//	}

//	public virtual void OnFixedUpdate()
//	{
//
//	}

//	public virtual void SetLocation(Vector2 where) { SetLocation (where.x, where.y); }
//	public virtual void SetLocation(float x, float y)
//	{
//		transform.localPosition = new Vector3 (x, y, 0);
//		transform.position = new Vector3 (transform.position.x, transform.position.y, GetZLocation ());
//	}

	public virtual ZLayers GetZLayer()
	{
		return ZLayers.None;
	}

	public float GetZLocation()
	{
		float r = ZLayerMod;
		r  += (int)GetZLayer() * 25;


		return r;
	}

	public enum ZLayers
	{ //Must have at least 10 units between each depth
		None,
		Camera,//-50
		Menu,//0
		Foreground,//15
		Structure,//25
		Player,//50
		WallThings//90
	}

	public virtual void OnDestroy()
	{
		
	}

	void Reset(){
		OnReset ();
	}

	public virtual void OnReset(){
		
	}

	//Gets called when you drop a CleanerSprite on a gameobject.
	public virtual void OnCleanup(){
		Debug.Log ("I CLEANED UP: " + name);
	}

	public void BecomeImage(){
		List<Transform> go = new List<Transform> {transform};
		while (go.Count > 0) {
			Transform a = go [0];
			go.Remove (a);
			for (int n = 0; n < a.childCount; n++)
				go.Add(a.GetChild (n));
			Rigidbody2D rb = a.GetComponent<Rigidbody2D> ();
			if (rb != null) Destroy (rb);
			Collider2D c = a.GetComponent<Collider2D> ();
			if (c != null) Destroy (c);
			Thing t = a.GetComponent<Thing> ();
			if (t != null) Destroy (t);
		}
	}

	public virtual void Destruct(){
		gameObject.SetActive(false);
		Invoke("DestroyREAL",1);
//		Destroy (gameObject);
	}

	void DestroyREAL()
	{
		Destroy (gameObject);
	}
}
