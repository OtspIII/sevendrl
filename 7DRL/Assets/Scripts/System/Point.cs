﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PRect
{
	public float x;
	public float y;
	public float w;
	public float h;
	public static PRect Zero { get { return new PRect (0, 0,0,0); } }

	public PRect (float X, float Y,float W, float H)
	{
		x = X;
		y = Y;
		w = W;
		h = H;
	}

	public override string ToString()
	{
		return "PRect(" + x + "," + y + "," + w + "," + w + ")";
	}
}

[System.Serializable]
public struct Point
{
	public int x;
	public int y;
	public static Point Zero { get { return new Point (0, 0); } }
	public static Point One { get { return new Point (1, 1); } }
	public static Point Up { get { return new Point (0, 1); } }
	public static Point Down { get { return new Point (0, -1); } }
	public static Point Left { get { return new Point (-1, 0); } }
	public static Point Right { get { return new Point (1, 0); } }
	public static Point Invalid { get { return new Point (-999, -999); } }
	
	public Point (int X, int Y)
	{
		x = X;
		y = Y;
	}

	public int Distance(Point other)
	{
		return Mathf.Abs (x - other.x) + Mathf.Abs (y - other.y);
	}


	public static Point operator +(Point c1, Point c2) 
	{
		return new Point(c1.x + c2.x, c1.y + c2.y);
	}
	public static Point operator -(Point c1, Point c2) 
	{
		return new Point(c1.x - c2.x, c1.y - c2.y);
	}
	public static Point operator *(Point c1, int n) 
	{
		return new Point(c1.x * n, c1.y * n);
	}
	public static Point operator /(Point c1, int n) 
	{
		return new Point(c1.x / n, c1.y / n);
	}
	public static bool operator ==(Point c1, Point c2) 
	{
		return c1.x == c2.x && c1.y == c2.y;
	}
	public static bool operator !=(Point c1, Point c2) 
	{
		return c1.x != c2.x || c1.y != c2.y;
	}
	public override bool Equals (object obj)
	{
		return base.Equals (obj);
	}
	public override int GetHashCode ()
	{
		return base.GetHashCode ();
	}
	public override string ToString ()
	{
		return "[Point: x: " + x.ToString() + ", y: " + y.ToString() + "]";
	}

	public Vector2 ToVector()
	{
		return new Vector2(x,y);
	}
}
