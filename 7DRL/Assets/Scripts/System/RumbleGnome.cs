﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RumbleGnome : MonoBehaviour {

	public float Intensity;
	public float Decrease;
	Vector3 StartPos;
	public Transform Target;

	public void Setup(Transform t, float i=0.7f, float d=1){
		Target = t;
		StartPos = t.localPosition;
		Intensity = i;
		Decrease = d;
	}

	void Update(){
		if (Intensity <= 0) {
			End ();
		}
		Target.localPosition = StartPos + (Random.insideUnitSphere * Intensity);
		Intensity -= Time.deltaTime * Decrease;
	}

	public void End(){
		Target.localPosition = StartPos;
//		if (God.GSM != null && God.GSM.RumbleG == this)
//			God.GSM.RumbleG = null;
		Destroy (gameObject);
	}
}
