﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpritePulser : MonoBehaviour {

	public SpriteRenderer SR;
	public float Speed = 0.5f;
	float Timer = 0;

	void Update(){
		Timer += Time.deltaTime * Mathf.PI * Speed / 2;
		float a = Mathf.Abs(Mathf.Sin (Timer));
		SR.color = new Color (1, 1, 1, a);
	}

	void Reset(){
		SR = GetComponent<SpriteRenderer> ();
	}
}
