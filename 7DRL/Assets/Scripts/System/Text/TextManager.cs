﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextManager : MonoBehaviour {

	public DynamicText DTPrefab;
	public TextLine TLPrefab;
//	public string Text;
	public List<List<string>> ParsedText;
	public List<TextLine> Lines;
	protected float RevealSpeed = 0.03f;
	protected float PeriodRevealSpeed = 0.1f;
	public bool HurryUp = false;
	public bool MidReveal = false;
	public int TextSize = 80;
	public float FadeOutTime = 0.5f;
	public Color DefaultColor;
	public Color CurrentColor;
	protected bool JustRevealedPeriod = false;
	Coroutine CurrentReveal;

	public void Setup(string text) { Setup (text, TextSize, true,true); }
	public void Setup(string text, bool hide) { Setup (text, TextSize, hide,true); }
	public void Setup(string text, bool hide, bool icons) { Setup (text, TextSize, hide,icons); }
	public void Setup(string text, int size, bool hide,bool icons)
	{
		Wipe ();
		CurrentColor = DefaultColor;
		ParsedText = ParseText (text,icons);
		TextSize = size;
		float offset = 0;
		foreach (List<string> l in ParsedText)
		{
			TextLine tl = Instantiate (TLPrefab).GetComponent<TextLine> ();
			tl.Setup (this);
			Lines.Add (tl);
			tl.transform.parent = transform;
			Vector3 w = new Vector3 (0, offset, 0);
			foreach (string t in l)
			{
				if (t.Length == 2 && t.Substring (0, 1) == "#")
				{
//					CurrentColor = God.Library.GetTextColor (t,DefaultColor);
				} else
				{
					DynamicText dt = Instantiate (DTPrefab).GetComponent<DynamicText> ();
//					if (t.Length > 1)
//						dt.Setup (God.Library.GetIcon (t), this);
//					else
//						dt.Setup (t, this);
					tl.AddText (dt);
				}
			}
//			w.x = 0 - (tl.GetWidth() / 2);
			tl.transform.localPosition = w;
			tl.transform.localScale = new Vector3 (1, 1, 1);
			offset -= tl.GetHeight ();
		}
		if (hide)
		{
			HideText ();
			CurrentReveal = StartCoroutine (Reveal ());
		}
	}

	public List<List<string>> ParseText(string text,bool icons)
	{
		List<string> line = new List<string> ();
		List<List<string>> r = new List<List<string>> ();
		string c = "";
		bool color = false;
		for (int n = 0; n < text.Length; n++)
		{
			c += text.Substring (n, 1);
			if (c == "#")
			{
				color = true;
				continue;
			}
			else if (c == "/" && icons)
				continue;
			else if (c.Length > 1)
			{
				if (color || text.Substring (n, 1) == "/")
				{
					line.Add (c);
					c = "";
					color = false;
				} else
					continue;
					
			}
			else if (c == ";")
			{
				r.Add (line);
				line = new List<string> ();
				c = "";
			}
			//If it's code for a symbol then do that instead, too.
			else
			{
				line.Add (c);
				c = "";
			}
		}
		r.Add (line);
		return r;
	}

	public void HideText()
	{
		foreach (TextLine tl in Lines)
			foreach (DynamicText dt in tl.Text)
				dt.gameObject.SetActive (false);
	}

	public IEnumerator Reveal()
	{
		MidReveal = true;
		float count = 0;
		foreach (TextLine tl in Lines)
			foreach (DynamicText dt in tl.Text)
			{
				if (dt.Text != null && (dt.Text.text == "." || dt.Text.text == "!" || dt.Text.text == "?"))
					JustRevealedPeriod = true;
				dt.FadeIn ();
				while (count < GetRevealSpeed ())
				{
					count += Time.deltaTime;
					yield return null;
					if (IM.Confirm())
						HurryUp = true;
				}
				count -= GetRevealSpeed ();
				JustRevealedPeriod = false;
			}
		MidReveal = false;
		HurryUp = false;
	}

	public IEnumerator Fade()
	{
		foreach (TextLine tl in Lines)
			foreach (DynamicText dt in tl.Text)
			{
				dt.FadeOut ();
			}
		yield return new WaitForSeconds (FadeOutTime);
	}

	public void Wipe()
	{
		if (CurrentReveal != null) {
			StopCoroutine (CurrentReveal);
		}
		while (Lines.Count > 0)
		{
			TextLine tl = Lines [0];
			Lines.Remove (tl);
			foreach (DynamicText dt in tl.Text)
				dt.Doomed = true;
			Destroy (tl.gameObject);
		}
	}

	float GetRevealSpeed()
	{
		if (!MidReveal)
			return 0;
		if (HurryUp)
			return 0.005f;
		if (JustRevealedPeriod)
			return PeriodRevealSpeed;
		return RevealSpeed;
	}
}
