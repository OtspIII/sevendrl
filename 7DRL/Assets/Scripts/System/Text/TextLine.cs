﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextLine : MonoBehaviour {

	TextManager TM;
	public List<DynamicText> Text;

	public void Setup(TextManager tm)
	{
		TM = tm;
	}

	public void AddText(DynamicText dt)
	{
		Vector3 offset = new Vector3( GetWidth (),0,0);
		Text.Add (dt);
		dt.transform.parent = transform;
		if (!dt.IsText)
		{
			offset.x += dt.Img.bounds.extents.x;
			offset.y -= dt.Img.bounds.extents.y;
		}
		dt.transform.localPosition = offset;
	}

	public float GetHeight()
	{
		float biggest = TM.TextSize / 350f;
		foreach (DynamicText dt in Text)
		{
			float h = dt.GetHeight ();
			biggest = Mathf.Max (biggest, h);
		}
//		Debug.Log ("b: " + biggest);
		return biggest;
	}

	public float GetWidth()
	{
		float r = 0;
		foreach (DynamicText dt in Text)
			r += dt.GetWidth ();
		return r;
	}
}
