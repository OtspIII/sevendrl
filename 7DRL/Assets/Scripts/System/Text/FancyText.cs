﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//using UnityEngine.EventSystems;
//
//[RequireComponent(typeof(TextMeshPro))]
//[RequireComponent(typeof(BoxCollider2D))]
//public class FancyText : UIThing
//{
//    public bool MenuCamera;
//    public TextMeshPro TM;
//    public Txt Text;
//    public BoxCollider2D Coll;
//    public string HBLink = "";
//    public Concept Target;
//    public bool FixedCollider;
////    int JustFailed;
//    public string text
//    {
//        get { return Text.Text; }
//        set { Text = new Txt(value); }
//    }
//
//    float hover = 0;
//
//    public void Setup(string t)
//    {
//        string oldT = TM.text;
//        Text = new Txt(t);
//        TM.text = Text.Text;
//        if (oldT != Text.Text)
//            Invoke("ColliderUpdate",0.01f);
//    }
//    
//    public void Setup(Txt t)
//    {
//        string oldT = TM.text;
//        Text = t;
//        TM.text = Text.Text;
//        if (oldT != Text.Text)
//            Invoke("ColliderUpdate",0.01f);
//    }
//
//    void ColliderUpdate()
//    {
//        if (FixedCollider)
//            return;
////        Debug.Log("A: " + TM.text);
//        if (string.IsNullOrEmpty(TM.text))
//        {
////            Debug.Log("B: " + TM.text);
//            Coll.size = new Vector3(0, 0, 1);
//        }
//        else
//        {
////            Debug.Log("C: " + TM.text);
////            Coll.size = Vector2.one;
////            Coll.offset = Vector2.zero;
//            Coll.size = TM.mesh.bounds.size;
//            Coll.offset = TM.mesh.bounds.center;
//        }
////        Debug.Log("D: " + " / " + Coll.size + " / " + TM.text + " / " + Text.Text);
//        if (Coll.size.x > 2000)
//            Coll.size = new Vector3(0, 0, 1);
//        if (Coll.size.x < 0.01f && Text.Text != "")
//        {
//            Coll.size = TM.bounds.size;
////            Debug.Log("WENT TO DEFAULT: " + Text.Text);
////            if (JustFailed > 2)
////                Debug.Log("TINY FANCY TEXT BOX: " + Coll.size + " / " + Time.time + " / " + Text.Text);
////            JustFailed++;
////            Invoke("ColliderUpdate", 0.15f);
//        }
////        else
////            JustFailed = 0;
//
//        //Coll.size = TM.mesh.bounds.size;
//    }
//
//    private void Reset()
//    {
//        TM = GetComponent<TextMeshPro>();
//        Coll = GetComponent<BoxCollider2D>();
//    }
//
//    public override bool ImprintOnHover(CardUIThing c)
//    {
////        Debug.Log("FTIoH: " + Target + " / " + c);
//        if (Target == null)
//            return false;
//        return Target.Imprint(c);
//    }
//
//
////    private void OnMouseDown()
////    {
////        int linkIndex = TMP_TextUtilities.FindIntersectingLink(TM, Input.mousePosition, Cam());
////        if( linkIndex != -1 ) { // was a link clicked?
////            
////            TMP_LinkInfo linkInfo = TM.textInfo.linkInfo[linkIndex];
////            
////        }
////    }
//
//    public void HoverOff()
//    {
//        God.GM.HB.TurnOff();
//        HBLink = "";
//        Target = null;
//        hover = 0;
//    }
//
//    void Click(string link)
//    {
////        Debug.Log("B: " + link);
//        HBLink = link;
//        God.GM.HB.TurnOn(link,Cam().ScreenToWorldPoint(Input.mousePosition));
//    }
//
//    public Camera Cam()
//    {
//        Camera r = MenuCamera ? God.MM?.Camera : God.Camera?.Camera;
//        return r != null ? r : God.GetCam();
//    }
//
//    protected override void OnMouseExit()
//    {
//        base.OnMouseExit();
//        if (HBLink == "")
//            return;
//        HoverOff();
//        
//    }
//
//    private void OnMouseOver()
//    {
//        int linkIndex = TMP_TextUtilities.FindIntersectingLink(TM, Input.mousePosition, Cam());
//        if (linkIndex != -1)
//        {
//            TMP_LinkInfo linkInfo = TM.textInfo.linkInfo[linkIndex];
//            string link = linkInfo.GetLinkID();
//            string txt = link;
//            if (HBLink != "" && link != HBLink)
//                HoverOff();
//            if (link.Length > 1)
//            {
//                char type = link[0];
//
//                switch (type)
//                {
//                    case '!':
//                        Target = Profile.GetActor(new Guid(link.Substring(1)));
//                        txt = "";
//                        break;
//                    case '#':
//                        Target = Profile.GetQuest(new Guid(link.Substring(1)));
//                        txt = "";
//                        break;
//                    case '@':
//                        Target = Profile.GetAction(new Guid(link.Substring(1)));
////                        Target = Prefabs.GetAction((Actions)Enum.Parse(typeof(Actions),link.Substring(1)));
//                        txt = "";
//                        break;
//                }
//            }
////            Debug.Log("A");
//            hover += Time.deltaTime;
//            if (hover > GameSettings.HoverRevealTime || (Input.GetMouseButtonDown(0) && txt != ""))
//                Click(txt);
//        }else
//            HoverOff();
////        Debug.Log("OMO: " + linkIndex + " / " + Target);
//    }
//}
//
//[System.Serializable]
//public class Txt
//{
//    public string Text;
//
//    public Txt(params object[] txt)
//    {
//        foreach (object o in txt)
//        {
//            Add(o);
//        }
//    }
//
//    public static string Hovertext(string txt,string hovertxt)
//    {
//        return "<color=#004a00><link=\"" + hovertxt + ">" + txt + "</link></color>";
//    }
//    
//    public static string MakeString(object o, Actor context=null)
//    {
//        if (o is Actor)
//        {
//            Actor a = (Actor) o;
//            return "<color=#033e82><link=\"!" + a.ID +"\">" + a.GetName()+"</link></color>";
//        }
//        else if (o is Quest)
//        {
//            Quest a = (Quest) o;
//            return "<color=#1e0152><link=\"#" + a.ID +"\">" + a.Name+"</link></color>";
//        }
//        else if (o is CharAction)
//        {
//            CharAction a = (CharAction) o;
//            return "<color=#00615a><link=\"@" + a.ID +"\">" + a.HeadText+"</link></color>";
//        }
//        else if (o is Trait)
//        {
//            Trait t = (Trait) o;
//            string txt = t.TagDesc != "" ? t.TagDesc : God.Niceify(t.Type.ToString());
//            string desc = t.HoverDesc;
//            if (txt == "")
//                return "";
//            if (desc != "")
//                txt = "<color=#004a00><link=\"" + desc + ">" + txt + "</link></color>";
//            return txt;
//        }
//        else if (o is CTags)
//        {
//            CTags tag = (CTags) o;
//            string desc = Tag.GetDesc(tag);
//            string txt = Tag.Get(tag,context);
//            if (txt == "")
//                return "";
//            if (desc != "")
//                txt = "<color=#004a00><link=\"" + desc + ">" + txt + "</link></color>";
//            return txt;
//        }
//        else if (o is Encounter)
//        {
//            Encounter c = (Encounter) o;
//            return c.Name;
//        }
//        else if (o is StatTrait)
//        {
//            StatTrait tag = (StatTrait) o;
//            string desc = tag.GetHoverDesc(context);
//            string txt = tag.GetName(context);
//            if (txt == "")
//                return "";
//            if (desc != "")
//                txt = "<color=#004a00><link=\"" + desc + ">" + txt + "</link></color>";
//            return txt;
//        }
//        else if (o is TownUpgrade)
//        {
//            TownUpgrade c = (TownUpgrade) o;
//            string desc = c.GetDesc();
//            return "<color=#004a00><link=\"" + desc + ">" + c.Name + "</link></color>";
//        }
//        else if (o is Concept)
//        {
//            Concept c = (Concept) o;
//            if (c.Name != "")
//                return c.Name;
//            return c.ToString();
//        }
//        else if (o != null)
//            return o.ToString();
//
//        return "";
//    }
//
//    public void Add(object o)
//    {
//        if (Text == "")
//            Text = "<color=black>";
//        Text += MakeString(o);
//        
//    }
//
//    public static Txt operator +(Txt txt, object o)
//    {
//        txt.Add(o);
//        return txt;
//    }
//
//    public override string ToString()
//    {
//        return Text;
//    }
//}