﻿using UnityEngine;
using System.Collections;

public class DynamicText : MonoBehaviour
{

	public TextManager TM;
	public TextMesh Text;
	MeshRenderer MR;
	public SpriteRenderer Img;
	public bool Doomed;

	public bool IsText { get { return Text != null; } }

	float CharacterSize = 0.1f;
	public Font Font;

	public void Setup (string text, TextManager tm)
	{
		TM = tm;
		Text = gameObject.AddComponent<TextMesh> ();
		MR = GetComponent<MeshRenderer> ();
		Text.font = Font;
		MR.material = Font.material;
		Text.characterSize = CharacterSize;
		Text.fontSize = TM.TextSize;
		Text.anchor = TextAnchor.UpperLeft;
		Text.text = text;
		Text.color = TM.CurrentColor;
	}

	public void Setup (Sprite sprite, TextManager tm)
	{
		TM = tm;
		Img = gameObject.AddComponent<SpriteRenderer> ();
		Img.sprite = sprite;
		float size = TM.TextSize / 80f;
		transform.localScale = new Vector3 (size, size, 1);
	}

	public float GetHeight ()
	{
		if (IsText)
			return MR.bounds.extents.y * 2;
		return Img.bounds.extents.y * 2;
	}

	public float GetWidth ()
	{
		if (IsText)
			return MR.bounds.extents.x * 2;
		return Img.bounds.extents.x * 2;
	}

	public void FadeIn ()
	{
		if (Doomed || gameObject == null)
			return;
		gameObject.SetActive (true);
		if ((IsText && Text == null) || (!IsText && Img == null)) {
			Debug.Log ("Quick kill text fadein.");
			return;
		}
		Color c = (IsText ? Text.color : Img.color);
		c.a = 0;
		if (IsText)
			Text.color = c;
		else
			Img.color = c;
		StartCoroutine (fadeIn (0.25f));
	}

	IEnumerator fadeIn (float time)
	{
		float t = 0;
		while (t < time) {
			t += Time.deltaTime;
			Color c = (IsText ? Text.color : Img.color);
			c.a = t / time;
			if (IsText)
				Text.color = c;
			else
				Img.color = c;
			yield return null;
		}
	}

	public void FadeOut ()
	{
		StartCoroutine (fadeOut (0.25f));
	}

	IEnumerator fadeOut (float time)
	{
		float t = 0;
		while (t < time) {
			t += Time.deltaTime;
			Color c = (IsText ? Text.color : Img.color);
			c.a = 1 - (t / time);
			if (IsText)
				Text.color = c;
			else
				Img.color = c;
			yield return null;
		}
		gameObject.SetActive (false);
	}
}
