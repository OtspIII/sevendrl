﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine.UI;

public static class God
{
	public static GM GM;
	public static float TimeScale;
	public static float TimeDefault;
	public static Directions[] Cardinal = new[] {Directions.Up, Directions.Right, Directions.Down, Directions.Left}; 
	public static Directions[] EightDir = new[] {Directions.Up, Directions.Right, Directions.Down, Directions.Left,Directions.UR,Directions.UL,Directions.DL,Directions.DR};
	public static Inputs[] KeyNums = new[] {Inputs.K1, Inputs.K2, Inputs.K3, Inputs.K4, Inputs.K5, Inputs.K6};
	
	

//	public static void PlaySound(SoundEffect se)
//	{
////		Debug.Log("PLAY SOUND: " + se);
//		Audio?.PlaySound(se);
//	}

	public static Directions OppositeDirection(Directions d)
	{
		switch (d)
		{
			case Directions.Up: return Directions.Down;
			case Directions.Down: return Directions.Up;
			case Directions.Left: return Directions.Right;
			case Directions.Right: return Directions.Left;
		}

		return Directions.None;
	}

	public static float GetTime(bool player=false)
	{
		if (player)
			return Time.deltaTime;
		return Time.deltaTime * TimeScale;
	}

	public static Color GetColor(Colors c)
	{
		switch (c)
		{
			case Colors.Black: return Color.black;
			case Colors.White: return Color.white;
			case Colors.Red: return Color.red;
			case Colors.Gray: return Color.gray;
			case Colors.Green: return Color.green;
			case Colors.Yellow: return Color.yellow;
			case Colors.Cyan: return Color.cyan;
			case Colors.Pink: return Color.magenta;
			case Colors.Blue: return Color.blue;
			
			case Colors.Orange: return new Color(1, 174f/255, 0);
			case Colors.Purple: return new Color(136f/255, 0, 1);
			case Colors.Lime: return new Color(0.8f, 1, 0);
			
			case Colors.Clear: return Color.clear;
		}
		return Color.white;
	}

	public static bool CoinFlip(float odds=0.5f)
	{
		return UnityEngine.Random.value < odds;
//		bool[] list = { true, false };
//		return list [God.Random(list.Length)];
	}

	public static void Error(string msg) { Error (null, msg); }
	public static void Error(Thing t, string msg)
	{
		string error = "[ERROR]";
		if (t != null)
			error += " " + t.name;
		error += ": ";
		error += msg;
		Debug.Log (error);
	}

	public static Vector2 MouseLoc(Camera cam=null)
	{
		if (cam == null)
			cam = UnityEngine.Camera.main;
		return cam.ScreenToWorldPoint(Input.mousePosition);
	}

	public static Vector2 CropRange(WorldThing start, Vector2 target, float range)
	{
		Vector2 s = start.transform.position;
		Vector2 mouse = target;
//		if (Vector2.Distance(mouse, s) < range)
//			return mouse;
		Vector2 dir = Vector2.MoveTowards(s,mouse,range);
		return dir;
	}

	public static float LookAtRot(Transform start, Vector3 end)
	{
		Vector2 dir = end - start.position;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		return Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles.z;
	}
	
	///If you need to artificially create an error, call this function
	public static void ThrowError()
	{
		int n = new int[1] {0}[4];
		 
	}


	public static Point Dir2Point(Directions d)
	{
		switch (d) {
		case Directions.Down:
			return new Point (0, -1);
		case Directions.Up:
			return new Point (0, 1);
		case Directions.Left:
			return new Point (-1, 0);
		case Directions.Right:
			return new Point (1, 0);
		case Directions.UL:
			return new Point (-1, 1);
		case Directions.UR:
			return new Point (1, 1);
		case Directions.DL:
			return new Point (-1, -1);
		case Directions.DR:
			return new Point (1, -1);
		}
		return new Point(0,0);
	}

	public static Directions GetDir(Vector2 start, Vector2 end, bool allowEightDir=false)
	{ return GetDir(end-start, out Directions b,allowEightDir); }

	public static Directions GetDir(Vector2 start, Vector2 end, out Directions backup, bool allowEightDir=false)
	{ return GetDir(end - start, out backup,allowEightDir); }

	public static Directions GetDir(Vector2 dir, bool allowEightDir=false)
	{ return GetDir(dir, out Directions b,allowEightDir); }

	public static Directions GetDir(Vector2 dir, out Directions backup, bool allowEightDir=false){
		dir = dir.normalized;
		backup = Directions.None;
//		Debug.Log("DIR: " + start + " / " + end + " / " + dir);
		if (dir.x >= Mathf.Abs(dir.y))
		{
			if (dir.y > 0) backup = Directions.Up;
			else if (dir.y < 0) backup = Directions.Down;
			if (allowEightDir && backup == Directions.Up) return Directions.UR;
			if (allowEightDir && backup == Directions.Down) return Directions.DR;
			return Directions.Right;
		}

		if (dir.x <= -Mathf.Abs(dir.y))
		{
			if (dir.y > 0) backup = Directions.Up;
			else if (dir.y < 0) backup = Directions.Down;
			if (allowEightDir && backup == Directions.Up) return Directions.UL;
			if (allowEightDir && backup == Directions.Down) return Directions.DL;
			return Directions.Left;
		}

		if (dir.y >= Mathf.Abs(dir.x))
		{
			if (dir.x > 0) backup = Directions.Right;
			else if (dir.x < 0) backup = Directions.Left;
			if (allowEightDir && backup == Directions.Right) return Directions.UR;
			if (allowEightDir && backup == Directions.Left) return Directions.UL;
			return Directions.Up;
		}

		if (dir.y <= -Mathf.Abs(dir.x))
		{
			if (dir.x > 0) backup = Directions.Right;
			else if (dir.x < 0) backup = Directions.Left;
			if (allowEightDir && backup == Directions.Right) return Directions.DR;
			if (allowEightDir && backup == Directions.Left) return Directions.DL;
			return Directions.Down;
		}

		return Directions.None;
	}

	public static float Ease(float t, bool inout)
	{
		if (inout)
			return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
		return (--t) * t * t + 1;
	}



	public static string Niceify(string t)
	{
		string r = "";
		for(int i = 0; i < t.Length; i++)
		{
			if(char.IsUpper(t[i]) && i != 0 && !char.IsUpper(t[i-1]))
				r += " ";
			r += t[i];
		}
		return r;
	}

	//Defaults to 'Up'
	public static Point Rotate (Point p, Directions d)
	{
		if (d == Directions.None)
			return Point.Zero;
		int x = p.x;
		int y = p.y;
		switch (d) {
		case Directions.Right:
			x = p.y;
			y = p.x * -1;
			break;
		case Directions.Down:
			x = p.x * -1;
			y = p.y * -1;
			break;
		case Directions.Left:
			x = p.y * -1;
			y = p.x;
			break;
		}
		
		return new Point (x, y);
	}

	public static Directions Rotate(Directions origin,Directions rotation)
	{
		Directions[] dirs = new[] {Directions.Up, Directions.Right, Directions.Down, Directions.Left};
		int o = Array.IndexOf(dirs, origin);
		o += Array.IndexOf(dirs,rotation);
		if (o > 3)
			o -= 4;
		else if (o < 0)
			return origin;
		return dirs[o];
	}

	public static List<Point> Outline(params Point[] shape)
	{
		List<Point> r = new List<Point>();
		foreach (Point p in shape)
		{
			foreach (Directions dir in God.EightDir)
			{
				Point w = p + God.Dir2Point(dir);
				if (!shape.Contains(w) && !r.Contains(w))
					r.Add(w);
			}
		}
		return r;
	}



	public static string Madlib(string txt, params string[] names){
		for (int n = 0; n < names.Length; n++)
		{
			if (!string.IsNullOrEmpty(names[n]))
				txt = txt.Replace("#" + n.ToString(), names[n]);
		}

		return txt;
	}

	public static int Random(int max){
		return UnityEngine.Random.Range(0,max);
	}

	public static float Random(float max){
		return UnityEngine.Random.Range(0,max);
	}

	
	//Extends lists to return a random entry
	public static T Random<T>(this List<T> l) where T:class
	{
		if (l.Count == 0)
			return null;
		return l[UnityEngine.Random.Range(0, l.Count)];
	}
	
	//Extends lists to return a random entry
	public static string Summary<T>(this List<T> l) 
	{
		string r = "";
		foreach (T t in l)
			r += (r == "" ? "" : " / ") + t.ToString();
		return "List: " + r;
	}
	
	//Extends lists to shuffle randomly
	public static List<T> Shuffle<T>(this List<T> l) where T:class
	{
		if (l.Count == 0)
			return l;
		List<T> temp = new List<T>();
		temp.AddRange(l);
		List<T> r = new List<T>();
		while (temp.Count > 0)
		{
			T chosen = temp.Random();
			temp.Remove(chosen);
			r.Add(chosen);
		}
		return r;
	}
	
	public static int Roll(int rolls, int size, int bonus=0){
		int r = 0;
		if (size > 0) {
			for (int n = 0; n < Mathf.Abs(rolls); n++)
				r += UnityEngine.Random.Range (1, size+1);
		}
		r += bonus;
		if (r < 0)
			r = 0;
		if (rolls < 0)
			return -r;
		return r;
	}

	public static T WeightedRandom<T>(Dictionary<T, float> dict) where T : class
	{
//		string debug = "WR [";
		float total = 0;
		foreach (float w in dict.Values)
			total += w;
//		foreach (T t in dict.Keys)
//			debug += t.ToString() + "." + dict[t] + " / ";
		float roll = UnityEngine.Random.Range(0f, total);
//		debug += roll + "::" + total;
		foreach (T opt in dict.Keys)
		{
			roll -= dict[opt];
			if (roll <= 0)
			{
//				Debug.Log(debug + " :: " + opt + "]");
				return opt;
			}
		}
		return null;
	}

	public static Dictionary<T, TU> Clone<T, TU>(this Dictionary<T, TU> d)
	{
		Dictionary<T,TU> r = new Dictionary<T, TU>();
		foreach(T k in d.Keys)
			r.Add(k,d[k]);
		return r;
	}

	public static List<T> Clone<T>(this List<T> l)
	{
		List<T> r = new List<T>();
		r.AddRange(l);
		return r;
	}
	
	public static int RoundRand(float n)
	{
		int r = (int) n;
		float rem = n - r;
		if (UnityEngine.Random.Range(0f, 1f) <= rem)
			r++;
		return r;
	}

	public static string Sign(int n)
	{
		if (n >= 0)
			return "+" + n;
		return n.ToString();
	}
	
	public static string Sign(float n)
	{
		if (n >= 0)
			return "+" + n;
		return n.ToString();
	}
	

	public static RectInt PointsToRect(params Point[] points)
	{
		int left = 9999;
		int right = -9999;
		int top = -9999;
		int bot = 9999;
		foreach (Point p in points)
		{
			left = Mathf.Min(left, p.x);
			right = Mathf.Max(right, p.x);
			bot = Mathf.Min(bot, p.y);
			top = Mathf.Max(top, p.y);
		}
		return new RectInt(left,bot,right - left + 1,top - bot + 1);
	}

	public static List<Point> RectToPoints(RectInt rect, bool startAtZero=true)
	{
		if (startAtZero)
		{
			rect.width--;
			rect.height--;
		}
//		Debug.Log("R->P: " + rect);
		List<Point> r = new List<Point>();
		for (int x = rect.x;x < rect.x + rect.width;x++)
		for (int y = rect.y; y < rect.y + rect.height; y++)
		{
//			Debug.Log("POINT: " + x + " / " + y);
			r.Add(new Point(x,y));
		}
		return r;
	}
}

public enum Directions{
	None,
	Up,
	Down,
	Left,
	Right,
	UL,
	UR,
	DL,
	DR
}