﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IM {

//	public static bool ForceMenu = false;
	public static Dictionary<Inputs,InputEntry> Ins = new Dictionary<Inputs, InputEntry>();
	public static bool Setup = false;

	public static void Init()
	{
		if (Setup)
			return;
		Setup = true;
		AddInput(new InputEntry(Inputs.MoveUp,KeyCode.UpArrow,KeyCode.W));
		AddInput(new InputEntry(Inputs.MoveLeft,KeyCode.LeftArrow,KeyCode.A));
		AddInput(new InputEntry(Inputs.MoveRight,KeyCode.RightArrow,KeyCode.D));
		AddInput(new InputEntry(Inputs.MoveDown,KeyCode.DownArrow,KeyCode.S));
		AddInput(new InputEntry(Inputs.Escape, KeyCode.Escape));
		AddInput(new InputEntry(Inputs.ChooseAction,KeyCode.Space).AddDesc("Hold "));
		AddInput(new InputEntry(Inputs.SpeedUp,KeyCode.LeftShift).AddDesc("Hold "));

		AddInput(new InputEntry(Inputs.MapGrab).AddMouse(1));
		AddInput(new InputEntry(Inputs.Menu,KeyCode.Tab));
		AddInput(new InputEntry(Inputs.Confirm, KeyCode.Space).AddMouse(0));
		AddInput(new InputEntry(Inputs.K1,KeyCode.Alpha1).BeSilent());
		AddInput(new InputEntry(Inputs.K2,KeyCode.Alpha2).BeSilent());
		AddInput(new InputEntry(Inputs.K3,KeyCode.Alpha3).BeSilent());
		AddInput(new InputEntry(Inputs.K4,KeyCode.Alpha4).BeSilent());
		AddInput(new InputEntry(Inputs.K5,KeyCode.Alpha5).BeSilent());
		AddInput(new InputEntry(Inputs.K6,KeyCode.Alpha6).BeSilent());
		AddInput(new InputEntry(Inputs.K7,KeyCode.Alpha7).BeSilent());
		AddInput(new InputEntry(Inputs.K8,KeyCode.Alpha8).BeSilent());
		AddInput(new InputEntry(Inputs.K9,KeyCode.Alpha9).BeSilent());
		AddInput(new InputEntry(Inputs.K0,KeyCode.Alpha0).BeSilent());
		AddInput(new InputEntry(Inputs.RBLeft,KeyCode.Minus).BeSilent());
		AddInput(new InputEntry(Inputs.RBRIght,KeyCode.Equals).BeSilent());
	}

	public static void AddInput(InputEntry i)
	{
		Ins.Add(i.I,i);
	}
	
	public static bool Confirm()
	{
		return GetButtonDown(Inputs.Confirm);
	}

//	public static bool HitEscape(){
//		return Input.GetKeyUp (KeyCode.Escape);
//	}

	public static bool Menu(){
//		if (ForceMenu) {
//			ForceMenu = false;
//			return true;
//		}
		return GetButtonDown(Inputs.Menu);
	}

	public static bool Click(bool held=false,int n=0)
	{
		if (held)
			return Input.GetMouseButton(n);
		return Input.GetMouseButtonUp(n);
	}

	public static bool GetButtonDown(Inputs i)
	{
		if (!Ins.ContainsKey(i))
		{
			Debug.Log("LACKING INPUT: " + i);
			return false;
		}

		InputEntry ie = Ins[i];
		foreach(KeyCode kc in ie.Keys)
			if (Input.GetKeyDown(kc))
				return true;
		if (ie.Mouse != -1)
			if (Input.GetMouseButtonDown(ie.Mouse))
				return true;
		return false;
	}
	
	public static bool GetButtonUp(Inputs i)
	{
		if (!Ins.ContainsKey(i))
		{
			Debug.Log("LACKING INPUT: " + i);
			return false;
		}

		InputEntry ie = Ins[i];
		foreach(KeyCode kc in ie.Keys)
			if (Input.GetKeyUp(kc))
				return true;
		if (ie.Mouse != -1)
			if (Input.GetMouseButtonUp(ie.Mouse))
				return true;
		return false;
	}
	
	public static bool GetButton(Inputs i)
	{
		if (!Ins.ContainsKey(i))
		{
			Debug.Log("LACKING INPUT: " + i);
			return false;
		}

		InputEntry ie = Ins[i];
		foreach(KeyCode kc in ie.Keys)
			if (Input.GetKey(kc))
				return true;
		if (ie.Mouse != -1)
			if (Input.GetMouseButton(ie.Mouse))
				return true;
		return false;
//		switch (i)
//		{
//			case Inputs.MoveUp:
//				return Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
//			case Inputs.MoveLeft:
//				return Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
//			case Inputs.MoveRight:
//				return Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
//			case Inputs.MoveDown:
//				return Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
//			case Inputs.Escape:
//				return Input.GetKey(KeyCode.Escape) || Input.GetMouseButton(1);
//			case Inputs.Confirm:
//				return Input.GetKey (KeyCode.Space) || Input.GetMouseButton(0);
//			case Inputs.Examine:
//				return Input.GetKey(KeyCode.LeftControl);
//			case Inputs.Menu:
//				return Input.GetKey(KeyCode.Tab);
//			case Inputs.Interact:
//				return Input.GetKey(KeyCode.LeftShift);
//		}
//
//		return false;
	}

	public static string ControlsTxt()
	{
		string r = "";
		foreach (InputEntry ie in Ins.Values)
		{
			if (ie.Silent)
				continue;
			if (r != "")
				r += "\n";
			r += God.Niceify(ie.I.ToString()) + " - ";
			r += ie.Desc;
			bool first = true;
			foreach (KeyCode kc in ie.Keys)
			{
				if (first)
					first = false;
				else
					r += " OR ";
				
				r += God.Niceify(kc.ToString());
			}

			if (ie.Mouse != -1)
			{
				if (!first)
					r += " OR ";
				switch (ie.Mouse)
				{
					case 0:
						r += "Left Click";
						break;
					case 1:
						r += "Right Click";
						break;
				}
			}
		}
		return r;
	}
}

public class InputEntry
{
	public Inputs I;
	public List<KeyCode> Keys;
	public int Mouse = -1;
	public string Desc = "";
	public bool Silent = false;

	public InputEntry(Inputs i, params KeyCode[] keys)
	{
		I = i;
		Keys = new List<KeyCode>();
		foreach(KeyCode k in keys)
			Keys.Add(k);
	}

	public InputEntry AddDesc(string desc)
	{
		Desc = desc;
		return this;
	}
	
	public InputEntry AddMouse(int m)
	{
		Mouse = m;
		return this;
	}

	public InputEntry BeSilent()
	{
		Silent = true;
		return this;
	}
}

public enum Inputs
{
	None,
	MoveUp,
	MoveLeft,
	MoveRight,
	MoveDown,
	Escape,
	ChooseAction,
	SpeedUp,
	Confirm,
	Menu,
	MapGrab,
	K1,
	K2,
	K3,
	K4,
	K5,
	K6,
	K7,
	K8,
	K9,
	K0,
	RBLeft,
	RBRIght,
}
