﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsLoader : MonoBehaviour
{
	
	
	void Awake()
	{
		if (GameSettings.Setup)
			return;
		GameSettings.Setup = true;
	}
}

public static class GameSettings
{
	public static bool Setup;

	public static float ChooseSpeed = 0.01f;
	public static float FFSpeed = 0.3f;
	public static float ActSpeed = 0.6f; //0.2f;
	public static Point SlotSize = new Point(24,14);
	public const int Levels = 10;
}