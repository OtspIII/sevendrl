﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessageBox : MonoBehaviour
{
    public TextMeshPro Title;
    public TextMeshPro Desc;
//    public SpriteRenderer SR;

    public void Imprint(string title, string desc)
    {
        Title.text = title;
        Desc.text = desc;
//        SR.sprite = s;
    }

    public void Wipe()
    {
        Imprint("","");
    }
   
    public IEnumerator Flyby(float h=0.6f,bool endGame=false)
    {
//      Debug.Log("FLYBY");
        GM.GameMode mode = God.GM.Mode;
        God.GM.SetMode(GM.GameMode.LoadScene);
        God.GM.PlayerAct = null;
        transform.parent = God.GM.Cam.transform;
//        float h = 0.6f; //transform.localPosition.y;
        Vector3 start =new Vector3(-14,h,2);
        Vector3 targ =new Vector3(-0.5f,h,2);
        Vector3 end =new Vector3(14,h,2);
        float zoom = 0.2f;
        float t = 0;
        
        gameObject.SetActive(true);
//        God.PlaySound(SoundEffect.TutorialCard);
        while (t < 1)
        {
            t += Time.deltaTime / zoom;
            if (t > 1)
                t = 1;
            float prog = God.Ease (t, true);
            transform.localPosition = Vector3.Lerp (start, targ, prog);
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);

        while (!IM.Confirm())
            yield return null;
        t = 0;
//      God.PlaySound(SoundEffect.Flyby);
        while (t < 1)
        {
            t += Time.deltaTime / zoom;
            if (t > 1)
                t = 1;
            float prog = God.Ease (t, true);
            transform.localPosition = Vector3.Lerp (targ, end, prog);
            yield return null;
        }
        gameObject.SetActive(false);
        God.GM.SetMode(mode);
        if (endGame)
        {
            God.GM.LoadScene("Title");
        }
    }
}
