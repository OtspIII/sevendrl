﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoverAction : CharAction
{
    public RecoverAction(float d)
    {
        AllowMove = false;
        Duration = d;
    }

//    public override void Start(Actor who, Actor targ)
//    {
//        base.Start(who, targ);
//        PlayerAction = false;
//        Debug.Log("RECOVER: " + Duration);
//    }

    public override void Run(Actor who)
    {
        base.Run(who);
        if (Clock > Duration)
            who.SetAct(null);
    }
}


public class KnockbackAction : CharAction
{
    public KnockbackAction(float d, float amt)
    {
        AllowMove = true;
        Duration = d;
        Range = amt;
    }
    
    public KnockbackAction(float d, float amt, Vector2 source)
    {
        AllowMove = true;
        Duration = d;
        TargetV = source;
        Range = amt;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
//        PlayerAction = false;
        Vector2 me = who.transform.position;
        Vector2 dir = (me - TargetV).normalized * Range;
        TargetV = me + dir;
        who.SetColor(Colors.Red);
    }

    public override bool End(Actor who, bool forced = false)
    {
        who.SetColor(Colors.None);
        return base.End(who, forced);
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this,who,5);
        if (Clock > Duration)
            who.SetAct(null);
    }
}

public class VanishAction : CharAction
{
    private Color C;
    public VanishAction(Color c)
    {
        AllowMove = true;
        C = c;
    }
    
    public override bool End(Actor who, bool forced = false)
    {
        who.Destruct();
        return base.End(who, forced);
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        who.ChangeStat(Stats.HP, -God.GetTime());
        float perc = who.GetStat(Stats.HP) / who.GetStat(Stats.MaxHP);
        who.BG.color = Color.Lerp(Color.black,C, perc);
        if (who.GetStat(Stats.HP) <= 0)
            who.Destruct();
    }
}

public class StunAction : CharAction
{
    public StunAction(float d, float amt)
    {
        AllowMove = false;
        Duration = d;
//        TargetV = source;
        Range = amt;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
//        PlayerAction = false;
        Vector2 me = who.transform.position;
        Vector2 dir = (me - TargetV).normalized * Range;
        TargetV = me + dir;
        who.SetColor(Colors.Gray);
    }

    public override bool End(Actor who, bool forced = false)
    {
//        Debug.Log("STUN END");
        who.SetColor(Colors.None);
        who.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
        base.End(who, forced);
        return forced;
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this,who,5);
        float rot = Clock / Duration * 360;
//        Debug.Log("STUN: " + Clock + " / " + Duration + " / " + rot);
        who.transform.rotation = Quaternion.Euler(new Vector3(0,0,rot));
        if (Clock > Duration)
            who.SetAct(null);
    }
}

public class FrozenAction : CharAction
{
    public FrozenAction(float d)
    {
        AllowMove = false;
        Duration = d;
//        TargetV = source;
//        Range = amt;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
//        PlayerAction = false;
        Vector2 me = who.transform.position;
        Vector2 dir = (me - TargetV).normalized * Range;
        TargetV = me + dir;
        who.SetColor(Colors.Cyan);
        God.GM.SpawnHeadtext(who,"frozen",Colors.Cyan);
    }

    public override bool End(Actor who, bool forced = false)
    {
//        Debug.Log("STUN END");
        who.SetColor(Colors.None);
        who.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
        base.End(who, forced);
        return forced;
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        float rot = Clock / Duration * 360;
//        Debug.Log("STUN: " + Clock + " / " + Duration + " / " + rot);
        who.transform.rotation = Quaternion.Euler(new Vector3(0,0,rot));
        if (Clock > Duration)
            who.SetAct(null);
    }
}