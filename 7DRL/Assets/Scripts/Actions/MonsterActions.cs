﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;


public class ShootAction : CharAction
{
    public float RecoverTime;
    
    public ShootAction(float dur = 2, float rec=0.3f)
    {
        Duration = dur;
        RecoverTime = rec;
        Range = 7;
        DeathDesc = "$'s Arrow";
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        who.AP.Imprint(">");   
        SetTarget(who,true);
        Clock = Random.Range(0, Duration / 2); //Sligtly variable time to fire
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        who.AP.Rotate(GetTarget(), 3 * God.GetTime());
        if (Duration - Clock < 0.2f)
            who.AP.Imprint("*");
        if (Recover(who,RecoverTime))
        {
//            SetTarget(who,false);
//            Target = null;
//            TargetV = who.AP.transform.position;
//            Debug.Log("SHOOT PROJ");
            God.GM.SpawnProjectile("arrow",Colors.Gray,who,this,who.transform.position, who.AP.Text.transform.position,Factions.Monster,20,new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)));
        }
    }
}



public class AttackAction : CharAction
{
    public float SwingStart;
    public float Arc = 320;
    public float RecoverTime = 0.2f;
    public List<GMsg> ExtraMsg = new List<GMsg>();

    public AttackAction(float dur = 0.5f, float arc = 320,float range=3,string desc = "$'s Strike",float recover=0.2f,params GMsg[] extraMsg)
    {
        Name = "Attack";
        PivotSize = new Vector2(1.25f,0.5f);
        Range = range;
        Duration = dur;
        Arc = arc;
        RecoverTime = 0.2f;
        DeathDesc = desc;
        ExtraMsg.AddRange(extraMsg);
    }

    public AttackAction SetSymbol(string sym, Vector2 p)
    {
        Symbol = sym;
        PivotSize = p;
        return this;
    }


    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        SetTarget(who,true);
        
    }

    public override void Run(Actor who)
    {
        if (!WithinRange)
        {
            WithinRange = CA.GetInRange(this, who);
            if (WithinRange)
            {
                SetTarget(who,false);
                who.AP.Imprint(Symbol,PivotSize);
                SwingStart = God.LookAtRot(who.transform,TargetV);
            }
            else
                return;
        }
        base.Run(who);
        CA.Dash(this,who,Range);
        CA.Swing(this,who,SwingStart,Arc);
        Recover(who,RecoverTime,false);
    }

    public override void OnHit(Actor who, Actor other)
    {
        if (who.Faction != Factions.None && other.Faction == who.Faction)
            return;
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
        foreach(GMsg m in ExtraMsg)
            m.Send(other,who,this);
    }
}


public class ConeOfFlame : CharAction
{
    float[] Spread = new []{-15,0,15f};
    private float Shots = 3;
    private float Variance = 5;
    private string FlameName;
    private Colors C;
    public GMsg[] ExtraP;
    public float RecoverTime;
    
    public ConeOfFlame(int shots, int bullets, float var=5,float maxSpread=15,string flamename="Fire",Colors c=Colors.Red,float recoverTime=1.5f,params GMsg[] extraPayload)
    {
        Duration = 0.5f;
        Range = 5;
        Shots = shots+1;
        Variance = var;
        C = c;
        FlameName = flamename;
        RecoverTime = recoverTime;
        List<float> spr = new List<float>();
        DeathDesc = "$'s " + flamename;
        ExtraP = extraPayload;
        for (int n = 0; n < bullets; n += 2)
        {
//            Debug.Log("COF: " + n);
            if (n == 0)
                spr.Add(0);
            else
            {
                float amt = Mathf.Lerp(0, maxSpread, n / (bullets-1f));
                spr.Add(amt);
                spr.Insert(0,-amt);
            }
        }
//        Debug.Log("COF2: " + spr.Summary());
        Spread = spr.ToArray();
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        SetTarget(who,true);
    }

    void Shoot(Actor who)
    {
        SetTarget(who,false);
        Vector2 pos = who.transform.position;
        float center = God.LookAtRot(who.transform, GetTarget()) - 90;
//        Debug.Log("AIM: " + center + " / " + Time.time);
        List<GMsg> pay = new List<GMsg>(){new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).AddTags(MTags.LowKnockback)};
        pay.AddRange(ExtraP);
        foreach (float f in Spread)
        {
//            Debug.Log("FIRE: " + f + " / " + center);
            who.TarPivot.transform.rotation = Quaternion.Euler(new Vector3(0,0,center + f + Random.Range(-Variance,Variance)));
            God.GM.SpawnProjectile(FlameName,C,who,this,pos, who.TarTip.transform.position,Factions.Monster,5,pay.ToArray());
        }
    }

    public override void Run(Actor who)
    {
        if (!WithinRange)
        {
            WithinRange = CA.GetInRange(this, who);
            if (WithinRange)
            {
                SetTarget(who,false);
            }
            else
                return;
        }
        base.Run(who);
        T += God.GetTime(PlayerAction);
        if (Shots > 1 && T >= Duration / Shots)
        {
            Shoot(who);
            T = 0;
        }

        if (Clock >= Duration)
            Recover(who, RecoverTime);
    }
}

public class SlimeWanderAction : CharAction
{
    public SlimeWanderAction()
    {
        AllowMove = true;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        Vector2 pos = who.transform.position;
        TargetV = pos + new Vector2(Random.Range(-7,7),Random.Range(-7,7));
    }
    
    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Walk(this,who,0.5f);
        CA.LookAround(this,who);
        Tile t = who.GetTile();
        if (t != null)
        {
            if (t.Contents == null)
                God.GM.Spawn(Things.SlimeTrail, t);
            else if (t.Contents.Type == Things.SlimeTrail)
                t.Contents.ChangeStat(Stats.HP, 100);
        }
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        who.SetAct(null);
    }
}

public class SlimeAttackAction : CharAction
{
    public float SwingStart;
    public float Arc = 320;
    public float RecoverTime = 0.2f;

    public SlimeAttackAction(float dur = 0.5f, float arc = 320,float range=3,string desc = "$'s Strike",float recover=0.2f)
    {
        Name = "Attack";
        PivotSize = new Vector2(1.25f,0.5f);
        Range = range;
        Duration = dur;
        Arc = arc;
        RecoverTime = 0.2f;
        DeathDesc = desc;
    }

    public SlimeAttackAction SetSymbol(string sym, Vector2 p)
    {
        Symbol = sym;
        PivotSize = p;
        return this;
    }


    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        SetTarget(who,true);
        
    }

    public override void Run(Actor who)
    {
        Tile t = null;
        if (!WithinRange)
        {
            
            WithinRange = CA.GetInRange(this, who);
            if (WithinRange)
            {
                SetTarget(who,false);
                who.AP.Imprint(Symbol,PivotSize);
                SwingStart = God.LookAtRot(who.transform,TargetV);
            }
            else
            {
                t = who.GetTile();
                if (t != null)
                {
                    if (t.Contents == null)
                        God.GM.Spawn(Things.SlimeTrail, t);
                    else if (t.Contents.Type == Things.SlimeTrail)
                        t.Contents.ChangeStat(Stats.HP, 100);
                }
                return;
            }
        }
        base.Run(who);
        CA.Dash(this,who,Range);
        CA.Swing(this,who,SwingStart,Arc);
        t = who.GetTile();
        if (t != null)
        {
            if (t.Contents == null)
                God.GM.Spawn(Things.SlimeTrail, t);
            else if (t.Contents.Type == Things.SlimeTrail)
                t.Contents.ChangeStat(Stats.HP, 100);
        }
        Recover(who,RecoverTime,false);
    }

    public override void OnHit(Actor who, Actor other)
    {
        if (who.Faction != Factions.None && other.Faction == who.Faction)
            return;
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
    }
}

public class ChargeAction : CharAction
{
    public float SwingStart;
    public float RecoverTime = 0.2f;
    public float Steam = 1;
    public float MaxSpeed = 2;
    public float TurnSpeed;

    public ChargeAction(float maxSpeed=3,float turnSpeed=1f, string desc = "$'s Charge",float recover=2f,float range=8)
    {
        Name = "Attack";
        PivotSize = new Vector2(1.25f,0.5f);
        Range = range;
        RecoverTime = recover;
        DeathDesc = desc;
        MaxSpeed = maxSpeed;
        Symbol = "=>\n=>";
        PivotSize = new Vector2(1,1.1f);
        TurnSpeed = turnSpeed;
    }

    public ChargeAction SetSymbol(string sym, Vector2 p)
    {
        Symbol = sym;
        PivotSize = p;
        return this;
    }


    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        SetTarget(who,true);
        who.AP.Rotate(GetTarget());

    }

    public override void Run(Actor who)
    {
        if (!WithinRange)
        {
            WithinRange = CA.GetInRange(this, who);
            if (WithinRange)
            {
                SetTarget(who,true);
                who.AP.Imprint(Symbol,PivotSize);
                SwingStart = God.LookAtRot(who.transform,TargetV);
            }
            else
                return;
        }
        base.Run(who);
        Steam += God.GetTime();
        if (Steam > MaxSpeed)
            Steam = MaxSpeed;
        float dist = Vector2.Distance(who.transform.position, GetTarget());
        if (Vector2.Distance(who.APTip.transform.position,GetTarget()) < dist || dist > MaxSpeed * 4)
            who.AP.Rotate(GetTarget(), TurnSpeed * God.GetTime());
        else
            Hit.Clear();
        CA.Charge(this,who,Steam);
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        base.OnBump(who, other);
        who.SetAct(new RecoverAction(RecoverTime));
    }

    public override void OnHit(Actor who, Actor other)
    {
        if (who.Faction != Factions.None && other.Faction == who.Faction)
            return;
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
    }
}

public class BulletHellAction : CharAction
{
    private float RoF;
    private string ProjName;
    public Colors ProjC;
    private Things Wake;
    
    public BulletHellAction(float rof, string projName="Fire", Colors projC=Colors.Red,Things wake=Things.None)
    {
        RoF = rof;
        Range = 7;
        ProjC = projC;
        ProjName = projName;
        Wake = wake;
        DeathDesc = "$'s " + ProjName;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        Vector2 pos = who.transform.position;
        TargetV = pos + new Vector2(Random.Range(-7,7),Random.Range(-7,7));
    }
    
    public override void Run(Actor who)
    {
        base.Run(who);
        T += God.GetTime();
        if (T > 1 / RoF)
        {
            T -= 1 / RoF;
            if (God.GM.Player == null || Vector2.Distance(who.transform.position,God.GM.Player.transform.position) < 12)
                God.GM.SpawnProjectile(ProjName,ProjC,who,this,who.transform.position, who.AP.Text.transform.position,Factions.Monster,2,new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)));
        }
        if (who.GetStat(Stats.SP) > 0)
            CA.Walk(this,who,0.5f);
        if (Wake != Things.None)
        {
            Tile t = who.GetTile();
            if (t != null)
            {
                if (t.Contents == null)
                    God.GM.Spawn(Wake, t);
                else if (t.Contents.Type == Wake)
                    t.Contents.ChangeStat(Stats.HP, 100);
            }
        }
        who.AP.Rotate(who.AP.transform.rotation.eulerAngles.z + (God.GetTime() * 360));
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        who.SetAct(null);
    }
}

public class LChargeAction : CharAction
{
    public float SwingStart;
    public float RecoverTime = 0.2f;
//    public float Steam = 1;
    public float MaxSpeed = 2;
    public float WaitTime = 2f;

    public LChargeAction(float maxSpeed=10, string desc = "$'s Zap",float recover=1f,float range=6)
    {
        Name = "Attack";
        Range = range;
        RecoverTime = recover;
        DeathDesc = desc;
        MaxSpeed = maxSpeed;
        Symbol = "<";
        PivotSize = new Vector2(0.5f,0.5f);
    }

    public LChargeAction SetSymbol(string sym, Vector2 p)
    {
        Symbol = sym;
        PivotSize = p;
        return this;
    }


    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        SetTarget(who,true);

    }

    public override void Run(Actor who)
    {
        if (!WithinRange)
        {
            WithinRange = CA.GetInRange(this, who);
            if (!WithinRange)
                return;
        }

        if (T < WaitTime)
        {
            if (God.CoinFlip())
                who.AP.Imprint(Symbol,PivotSize);
            else
                who.AP.Imprint(">",PivotSize);
            T += God.GetTime();
            SetTarget(who,true);
            who.AP.Rotate(GetTarget());
//            SwingStart = God.LookAtRot(who.transform,TargetV);
            
            return;
        }
        who.AP.Imprint(Symbol,PivotSize);
        base.Run(who);
        CA.Charge(this,who,MaxSpeed);
    }

    public override void OnBump(Actor who, WorldThing other)
    {
        base.OnBump(who, other);
        who.SetAct(new RecoverAction(RecoverTime));
    }

    public override void OnHit(Actor who, Actor other)
    {
        if (who.Faction != Factions.None && other.Faction == who.Faction)
            return;
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
    }
}

public class BombToss : CharAction
{
    public float Splash;
    public string ProjName;
    public Colors C;
    public GMsg[] Payload;
    public float RecoverTime;
    
    public BombToss(string nam,Colors c,float recover=2f,float splash=5,float range=5,params GMsg[] payload)
    {
        ProjName = nam;
        Duration = 0.5f;
        RecoverTime = recover;
        Splash = splash;
        Range = range;
        DeathDesc = "$'s " + nam;
        C = c;
        Payload = payload;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        who.AP.Imprint("=");   
        SetTarget(who,true);
        
//        Vector2 dir = (GetTarget() - pos).normalized;
        
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        who.AP.Rotate(GetTarget());
        if (Duration - Clock < 0.2f)
            who.AP.Imprint("*");
        if (Recover(who, RecoverTime))
        {
            Vector2 pos = who.transform.position;
            God.GM.SpawnLob(ProjName,C, who,this,pos, GetTarget(),who.Faction,20,Splash,Payload);
        }
    }
}