﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class DashAction : CharAction
{
    public DashAction()
    {
        Name = "Dash";
        Range = 6;
        StaminaCost = 1;
        Duration = 0.5f;
//        Desc = "Quick Movement"
    }
    
    public override void Start(Actor who, Actor targ)
    {
        TargetV = God.CropRange(who,God.MouseLoc(),GetRange(who,1));
        base.Start(who,targ);
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this, who, 8);
        if (Vector2.Distance(who.transform.position,GetTarget()) < 0.01f)
            who.SetAct(null);
        if (Clock > Duration)
            who.SetAct(null);
    }
    
    public override void OnBump(Actor who, WorldThing other)
    {
        who.SetAct(null);
    }
}

public class SwingAction : CharAction
{
    public float SwingStart;
    public float Arc = 280;
    
    public SwingAction()
    {
        Name = "Swing";
        PivotSize = new Vector2(2.1f,1);
        Duration = 0.5f;
        StaminaCost = 2;
        Range = 5;
    }
    

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        who.AP.Imprint("===>",PivotSize);
        TargetV = God.CropRange(who,God.MouseLoc(),GetRange(who,0.75f));
        SwingStart = God.LookAtRot(who.transform,TargetV);
        
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this,who,6);
        CA.Swing(this,who,SwingStart,Arc);
        Recover(who,0.5f,false);
    }

    public override void OnHit(Actor who, Actor other)
    {
//        Debug.Log("HIT THEM");
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
    }
}

//public class PThrowRock : CharAction
//{
//    public PThrowRock()
//    {
//        Name = "Throw Rock";
//        Duration = 0.05f;
//        Range = 5;
//        Uses = new Point(2,4);
//    }
//
//    public override void Start(Actor who, Actor targ)
//    {
//        base.Start(who, targ);
//        Vector2 pos = who.transform.position;
//        Vector2 dir = (God.MouseLoc() - pos).normalized;
//        God.GM.SpawnProjectile("rock",who,pos, God.MouseLoc(),who.Faction,30,new GMsg(GMsgs.TakeAction).SetFAct(new StunAction(2,2)));
//    }
//
//    public override void Run(Actor who)
//    {
//        base.Run(who);
//        if (Clock > Duration)
//            who.SetAct(null);
//    }
//}

public class QSwingAction : RodAction
{
    public float SwingStart;
    public float Arc = 280;
    public GMsg[] Payload;
    
    public QSwingAction(params GMsg[] payload)
    {
        Name = "Quick Strike";
        PivotSize = new Vector2(2.1f,1);
        Duration = 0.5f;
        Range = 5;
        ResolveSpeed = GameSettings.ChooseSpeed;
        Uses = new Point(1,3);
        Payload = payload;
//        Rod = rod;
    }
    

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        who.AP.Imprint("===>",PivotSize);
        TargetV = God.CropRange(who,God.MouseLoc(),GetRange(who,0.5f));
        SwingStart = God.LookAtRot(who.transform,TargetV);
        
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this,who,6);
        CA.Swing(this,who,SwingStart,Arc);
        Recover(who,0.5f,false);
    }

    public override void OnHit(Actor who, Actor other)
    {
        if (Payload.Length == 0)
            new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM)).Send(other,who,this);
        else
            foreach (GMsg m in Payload)
            {
                new GMsg(m).Send(other,who,this);
            }
    }
}

public class PConeOfFlame : RodAction
{
    float[] Spread = new []{-15,0,15f};
    private float Shots = 3;
    
    public PConeOfFlame()
    {
        Name = "Flames";
        Duration = 0.5f;
        Range = 1;
        Uses = new Point(2,4);
        ResolveSpeed = 0.5f;
//        Rod = rod;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        Shoot(who);

    }

    void Shoot(Actor who)
    {
        Vector2 pos = who.transform.position;
        float center = God.LookAtRot(who.transform, God.MouseLoc()) - 90;
//        Debug.Log("AIM: " + center + " / " + Time.time);
        foreach (float f in Spread)
        {
            who.TarPivot.transform.rotation = Quaternion.Euler(new Vector3(0,0,center + f));
            God.GM.SpawnProjectile("fire",Colors.Red,who,this,pos, who.TarTip.transform.position,who.Faction,5,new GMsg(GMsgs.TakeDamage).SetAmount(2).AddTags(MTags.LowKnockback));
        }
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        T += God.GetTime(PlayerAction);
        if (Shots > 1 && T >= Duration / Shots)
        {
            Shoot(who);
            T = 0;
        }
        if (Clock >= Duration)
            who.SetAct(null);
    }
}

public class PotionToss : CharAction
{
    private PotionEntry Potion;
    
    public PotionToss(PotionEntry pot)
    {
        Potion = pot;
        Duration = 0.05f;
        PivotSize = new Vector2(pot.Splash/2,0);
        Range = 10;
        Uses = new Point(1,3);
    }

    public override string GetName()
    {
        return Potion.GetName();
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        Vector2 pos = who.transform.position;
        Vector2 dir = (God.MouseLoc() - pos).normalized;
        if (Potion.Splash > 0)
            God.GM.SpawnLob("potion",Potion.C, who,this,pos, God.MouseLoc(),who.Faction,20,Potion.Splash,Potion.Payload.ToArray());
        else
            God.GM.SpawnProjectile("potion",Potion.C, who,this,pos, God.MouseLoc(),who.Faction,20,Potion.Payload.ToArray());
        Potion.Identified = true;
    }

    public override void Preview(Actor who)
    {
        if (!Potion.Identified)
        {
            Vector2 range = God.CropRange(who, God.MouseLoc(),10);
            God.GM.SetCursor(range,0);
            return;
        }
        base.Preview(who);
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        if (Clock > Duration)
            who.SetAct(null);
    }
}

public class POgreAction : RodAction
{
    public float SwingStart;
    public float Arc = 280;
    
    public POgreAction()
    {
        PivotSize = new Vector2(2.5f,1);
        Symbol = "<&&&&";
        Duration = 0.5f;
        Range = 5;
        SymbolSize = 1.5f;
        Uses = new Point(1,2);
        
    }
    

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who,targ);
        who.AP.Imprint(Symbol,PivotSize);
        TargetV = God.CropRange(who,God.MouseLoc(),GetRange(who,0.75f));
        SwingStart = God.LookAtRot(who.transform,TargetV);
        who.AP.transform.localScale = new Vector3(SymbolSize,SymbolSize,SymbolSize);
        
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        CA.Dash(this,who,6);
        CA.Swing(this,who,SwingStart,Arc);
        Recover(who,0.5f,false);
    }

    public override bool End(Actor who, bool forced = false)
    {
        who.AP.transform.localScale = new Vector3(1,1,1);
        return base.End(who, forced);
    }


    public override void OnHit(Actor who, Actor other)
    {
//        Debug.Log("HIT THEM");
        new GMsg(GMsgs.TakeDamage).SetAmount(who.GetStat(Stats.DM) * 3).Send(other,who,this);
    }
}

public class PRodEffect : RodAction
{
    private float Splash = 5;
    public GMsg[] Payload;
    
    public PRodEffect(int uses,float splash=5,params GMsg[] payload)
    {
//        Name = nam;
        Duration = 0.1f;
        Range = 10;
        Uses = new Point(1,uses);
        ResolveSpeed = 0.1f;
        Splash = splash;
        Payload = payload;
//        Rod = rod;
    }

    public override void Start(Actor who, Actor targ)
    {
        base.Start(who, targ);
        List<Actor> hit = God.GM.SpawnExplosion(God.MouseLoc(), Factions.None, Splash);
        Vector2 where = God.MouseLoc();

        foreach (GMsg m in Payload)
        {
            foreach (Actor a in hit)
                new GMsg(m).Send(a, who, this);
            if (m.Type == GMsgs.Summon)
            {
                Things type = m.Thing;
                if (type == Things.None)
                    type = Library.RandomMonster();
                Tile til = God.GM.D.GetTile(where);
                if (til == null)
                    continue;
                Actor p = God.GM.Spawn(type, til);
                p.Faction = Factions.None;
                p.DefaultColor = Color.red;
                p.SetColor(Colors.None);
                if (type == Things.FireTrail)
                {
                    p.SetStat(Stats.DM, 5);
                    p.TagList.Remove(Tags.Projectile);
                    p.SetStat(Stats.MaxHP, 10);
                    p.SetStat(Stats.HP, 10);
                }
                else
                {
                    God.GM.SpawnHeadtext(p,"summoned",Colors.Red);
                }
            }
        }
    }

    public override void Run(Actor who)
    {
        base.Run(who);
        
        if (Clock >= Duration)
            who.SetAct(null);
    }
}
