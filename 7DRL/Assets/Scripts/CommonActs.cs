﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public static class CA
{
    public static void Dash(CharAction ca, Actor who, float speed=3)
    {
        
        float slow = God.GetTime(ca.PlayerAction);
        Vector2 loc = who.transform.position;
//        Debug.Log("DASH: " + (speed * slow) + " / " + slow);
        loc = Vector2.Lerp(loc, ca.GetTarget(), speed * slow);
        loc = Vector2.MoveTowards(loc, ca.GetTarget(), 0.1f * slow);
        who.RB.MovePosition(loc);
//        
//        
//
//        if (ca.Clock > 0.5f)
//        {
//            who.SetAct(null);
//        }
    }

    public static void Walk(CharAction ca, Actor who, float speed=1)
    {
        float slow = God.GetTime(ca.PlayerAction);
        Vector2 loc = who.transform.position;
        float amt = (who.GetStat(Stats.SP) * speed) * slow;
        loc = Vector2.MoveTowards(loc, ca.GetTarget(), amt);
        who.RB.MovePosition(loc);
        if (Vector2.Distance(loc,ca.GetTarget()) < 0.01f)
            who.SetAct(null);
        
        if (ca.Clock > 0.25f)
        {
            if (Vector2.Distance(ca.Loc, who.transform.position) / God.TimeScale <= 0.01)
                who.SetAct(null);
            ca.Clock = 0;
            ca.Loc = who.transform.position;
        }
    }
    
    public static void Charge(CharAction ca, Actor who, float speed=1)
    {
        float slow = God.GetTime(ca.PlayerAction);
        Vector2 loc = who.transform.position;
        float amt = (who.GetStat(Stats.SP) * speed) * slow;
        Vector2 dir = who.APTip.transform.position - who.transform.position;
        dir = dir.normalized * 10;
        loc = Vector2.MoveTowards(loc, loc + dir, amt);
        who.RB.MovePosition(loc);
        if (Vector2.Distance(loc,ca.GetTarget()) < 0.01f)
            who.SetAct(null);
        
        if (ca.Clock > 0.25f)
        {
            if (Vector2.Distance(ca.Loc, who.transform.position) / God.TimeScale <= 0.01)
                who.SetAct(null);
            ca.Clock = 0;
            ca.Loc = who.transform.position;
        }
    }

    public static void LookAround(CharAction ca, Actor who)
    {
        if (who.CanSee(God.GM.Player))
        {
            ActTracker a = who.ChooseAttack(God.GM.Player);
            if (a != null)
                who.SetAct(a);
        }
    }

    public static void Swing(CharAction ca, Actor who, float start, float arc)
    {
        ca.T += God.GetTime(ca.PlayerAction) / ca.Duration;
        float e = God.Ease(ca.T, true);
        if (ca.T <= 1)
        {
            
            who.AP.Rotate(start - (arc/2) + (e * arc));
        }
    }

    public static bool GetInRange(CharAction ca, Actor who)
    {
        if (Vector2.Distance(who.transform.position,ca.GetTarget()) > ca.Range)
            Walk(ca,who);
        return Vector2.Distance(who.transform.position, ca.GetTarget()) < ca.Range;
    }
}
