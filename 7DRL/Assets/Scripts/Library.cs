﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Runtime.CompilerServices;
using UnityEngine;

public static class Library
{

    public static bool Setup = false;
    public static Dictionary<RewardEntry,float> Rewards = new Dictionary<RewardEntry, float>();
    public static List<MonsterEntry> Monsters = new List<MonsterEntry>();
    public static List<Colors> RandomColors = new List<Colors>(){Colors.White,Colors.Gray,Colors.Red,Colors.Yellow,Colors.Blue,Colors.Pink,Colors.Purple,Colors.Cyan,Colors.Orange,Colors.Lime,Colors.Green};
    public static List<Colors> PotionColors = new List<Colors>();
    public static List<Colors> RodColors = new List<Colors>();
    public static List<PotionEntry> Potions = new List<PotionEntry>();
    public static List<RodEntry> Rods = new List<RodEntry>();

    public static void Init()
    {
        if (Setup)
            return;
        Setup = true;
        
        PotionColors.AddRange(RandomColors);
        RodColors.AddRange(RandomColors);
        
        Rewards.Add(new RewardEntry(Stats.MaxHP,2), 1);
        Rewards.Add(new RewardEntry(Stats.SP,0.5f),1);
        Rewards.Add(new RewardEntry(Stats.DM,1),1);
        Rewards.Add(new RewardEntry(Stats.MaxST,2),1);
        Rewards.Add(new RewardEntry(Stats.HP,10),1);
        
        //Strikers
        Monsters.Add(new MonsterEntry(Things.Goblin,           2,  2,  1,  1,.1f  ,0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Zombie,           0,  0,  1,  2,  2,  1,.1f,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Ogre,             0,  0,  0,  0,  1,  2,  2,  1,.1f,  0));
        Monsters.Add(new MonsterEntry(Things.Phantom,          0,  0,  0,  0,  0,  0,  0,  2,.1f,.1f));

        //Ranged
        Monsters.Add(new MonsterEntry(Things.Archer,            2,  2,  1,  1,.1f,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Kobold,          .1f,  1,  2,  2,  1,.1f,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Elf,               0,  0,  0,  0,  0,.1f,  1,  2,  2,  1));

        //Spray
        Monsters.Add(new MonsterEntry(Things.Beetle,          .5f,  1,  1,  0,  0,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Hellhound,         0,  0,.1f,  1,  2,  1,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.IceElemental,      0,  0,  0,  0,  0,  1,  2,  1,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Dragon,            0,  0,  0,  0,  0,  0,  0,  0,  1,  2));

        //Leap
        Monsters.Add(new MonsterEntry(Things.Rat,               2,  2,  1,  1,  0,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Jackal,            0,  0,  0,  1,  2,  2,  1,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.LightningElemental,0,  0,  0,  0,  0,  0,  1,  2,  2,  1));

        //Charge
        Monsters.Add(new MonsterEntry(Things.VampireBat,        1,  1,  1,  1,  0,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Quagga,            0,  0,  0,  1,  1,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Minotaur,          0,  0,  0,  0,  0,  1,  1,  1,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Unicorn,           0,  0,  0,  0,  0,  0,  0,.1f,  1,  2));
        Monsters.Add(new MonsterEntry(Things.Wraith,            0,  0,  0,  0,.1f,  1,  1,  1,  0,  0));

        //Bullet Hell        
        Monsters.Add(new MonsterEntry(Things.YellowMold,      .2f,.2f,.2f,.1f,  0,  0,  0,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.FlameElemental,    0,  0,  0,.1f,  1,  2,  1,  0,  0,  0));
        Monsters.Add(new MonsterEntry(Things.Nightmare,         0,  0,  0,  0,  0,  0,  0,  1,  2,  1));
        
        //Special
        Monsters.Add(new MonsterEntry(Things.Xeno,              0,  0,  0,  0,  0,  0,  0,  0,  0,.5f));
        Monsters.Add(new MonsterEntry(Things.TimeElemental,     0,  0,  0,  0,  0,.3f,.3f,.3f,.3f,  0));
        Monsters.Add(new MonsterEntry(Things.Cockatrice,        0,  0,  0,  0,.1f,.1f,.1f,.1f,.1f,.1f));
        Monsters.Add(new MonsterEntry(Things.Slime,             .3f,.3f,.3f,.3f,.3f,.3f,0,  0,  0,  0));

//        Dictionary<MonsterEntry, float> totals = new Dictionary<MonsterEntry, float>();
//        for (int n = 1; n <= GameSettings.Levels; n++)
//        {
//            Dictionary<MonsterEntry, float> mons = MonsterProfile(n);
//            float total = 0;
//            foreach (MonsterEntry e in mons.Keys)
//                total += mons[e];
//            string desc = "Level " + n + ": ";
//            foreach (MonsterEntry e in mons.Keys)
//            {
//                if (mons[e] <= 0)
//                    continue;
//                float perc = (mons[e] / total * 100);
//                desc += e.Type.ToString() + "(" + (int)perc + "%), ";
//                if (!totals.ContainsKey(e))
//                    totals.Add(e,0);
//                totals[e] += perc /5;
//            }
//            Debug.Log(desc);
//        }
//        string tot = "Totals: ";
//        foreach (MonsterEntry e in totals.Keys)
//        {
//            tot += e.Type.ToString() + "(" + totals[e]+"), ";
//        }
//        Debug.Log(tot);
        
        
//        Potions.Add(new PotionEntry("Poison",0,new GMsg(GMsgs.TakeDamage).SetAmount(10)));
//        Potions.Add(new PotionEntry("Stunning",0,new GMsg(GMsgs.TakeAction).SetFAct(new StunAction(2,2))));
//        Potions.Add(new PotionEntry("Knockback",5,new GMsg(GMsgs.TakeAction).SetFAct(new KnockbackAction(0.5f,10))));
//        Potions.Add(new PotionEntry("Fire",5,new GMsg(GMsgs.TakeDamage).SetAmount(5)));
//        Potions.Add(new PotionEntry("Petrification",3,new GMsg(GMsgs.Petrify)));
//        Potions.Add(new PotionEntry("Ice",5,new GMsg(GMsgs.TakeDamage).SetAmount(2),new GMsg(GMsgs.TakeAction).SetFAct(new FrozenAction(2f))));
//        Potions.Add(new PotionEntry("Noise",30,new GMsg(GMsgs.TakeDamage).SetAmount(1)));
//        Potions.Add(new PotionEntry("Healing",5,new GMsg(GMsgs.Heal).SetAmount(10)));
//        Potions.Add(new PotionEntry("Movement",5,new GMsg(GMsgs.Teleport)));
//        Potions.Add(new PotionEntry("Invisibility",5,new GMsg(GMsgs.Invisible)));
//        Potions.Add(new PotionEntry("Rage",5,new GMsg(GMsgs.Anger)));
        //Identify
        //Speed Up?
        //Speed Down?
        //Slime
        //Lava
        
//        Rods.Add(new RodEntry("Quick Strike",new QSwingAction()));
//        Rods.Add(new RodEntry("Flames",new PConeOfFlame()));
//        Rods.Add(new RodEntry("Ogre Strike",new POgreAction()));
//        Rods.Add(new RodEntry("Reflection",new QSwingAction(new GMsg(GMsgs.Reflect))));
        Rods.Add(new RodEntry("Petrify",new PRodEffect(1,1,new GMsg(GMsgs.Petrify))));
        Rods.Add(new RodEntry("Polymorph",new PRodEffect(2,3,new GMsg(GMsgs.Polymorph))));
        Rods.Add(new RodEntry("Summoning",new PRodEffect(2,3,new GMsg(GMsgs.Summon))));
//        Rods.Add(new RodEntry("Force",new QSwingAction(new GMsg(GMsgs.TakeAction).SetFAct(new KnockbackAction(0.5f,10)))));
        Rods.Add(new RodEntry("Lava",new PRodEffect(2,3,new GMsg(GMsgs.Summon).SetThing(Things.FireTrail))));
        Rods.Add(new RodEntry("Slime",new PRodEffect(2,3,new GMsg(GMsgs.Summon).SetThing(Things.SlimeTrail))));
//        Rods.Add(new RodEntry("Pain",new PRodEffect(8,2,new GMsg(GMsgs.TakeDamage).SetAmount(1))));
        //Charge
    }

    public static Things RandomMonster()
    {
        List<Things> maybe = new List<Things>();
        foreach(MonsterEntry me in Monsters)
            maybe.Add(me.Type);
        return maybe[Random.Range(0, maybe.Count)];
    } 

    public static Dictionary<MonsterEntry, float> MonsterProfile(int lvl)
    {
        Dictionary<MonsterEntry,float> r = new Dictionary<MonsterEntry, float>();
        foreach (MonsterEntry m in Monsters)
        {
            float w = m.Weights[lvl-1];
            r.Add(m,w);
        }

        return r;
    }

    public static PotionEntry GetPotion()
    {
        return Potions.Random();
    }
    
    public static GearEntry GetRandomGear()
    {
        List<GearEntry> options = new List<GearEntry>();
        options.AddRange(Potions);
        options.AddRange(Rods);
        return options.Random();
//        int options = Rods.Count + Library.Potions.Count;
//        if (God.CoinFlip())
//            r = new PotionToss();
//        else
//            r = Rods.Random().Act;
        
//        switch (God.Roll(1,options))
//        {
//            case 1: r = new PConeOfFlame(); break;
//            case 2: r = new QSwingAction(); break;
//            default: r = new PotionToss(); break;
//        }
    }
}

public class RewardEntry
{
    public Stats S;
    public float Amt;

    public RewardEntry(Stats s,float amt)
    {
        S = s;
        Amt = amt;
    }
}

public class MonsterEntry
{
    public Things Type;
    public float[] Weights = new float[GameSettings.Levels];

    public MonsterEntry(Things t,params float[] weights)
    {
        Type = t;
        for (int n = 0; n < weights.Length; n++)
        {
            if (n >= GameSettings.Levels)
                break;
            Weights[n] = weights[n];
        }
    }

    public void AddWeights(int start, params float[] weights)
    {
        for (int n = 0; n < weights.Length; n++)
        {
            if (n+start >= GameSettings.Levels)
                break;
            Weights[n+start] = weights[n];
        }
    }
}

public class GearEntry
{
    public string Name;
    public Colors C;
    public bool Identified = false;

    public virtual string GetName()
    {
        return "";
    }

    public virtual CharAction GetAction()
    {
        return null;
    }

    public virtual void Imprint(Actor who)
    {
        
    }
}

public class PotionEntry : GearEntry
{
    public List<GMsg> Payload = new List<GMsg>();
    public float Splash = 0;

    public PotionEntry(string name,float splash,params GMsg[] payload)
    {
        Name = name;
        Splash = splash;
        C = Library.PotionColors[Random.Range(0,Library.PotionColors.Count)];
        Library.PotionColors.Remove(C);
        Payload.AddRange(payload);
    }

    public override string GetName()
    {
        return Identified ? "Potion of " + Name : C.ToString() + " Potion";
    }

    public override CharAction GetAction()
    {
        return new PotionToss(this);
    }

    public override void Imprint(Actor who)
    {
        base.Imprint(who);
        who.Imprint('!',God.GetColor(C));
    }
}

public class RodEntry : GearEntry
{
    public RodAction Act;

    public RodEntry(string name, RodAction act)
    {
        Name = name;
        C = Library.RodColors[Random.Range(0,Library.RodColors.Count)];
        Library.RodColors.Remove(C);
        Act = act;
        Act.Rod = this;
    }
    
    public override string GetName()
    {
        return Identified ? "Rod of " + Name : C.ToString() + " Rod";
    }

    public override CharAction GetAction()
    {
        return Act;
    }
    
    public override void Imprint(Actor who)
    {
        base.Imprint(who);
        who.Imprint('/',God.GetColor(C));
    }
}
