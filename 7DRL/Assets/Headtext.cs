﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Headtext : MonoBehaviour
{
    public TextMeshPro Text;
    public float TimeLeft;
    
    public void Imprint(string txt, Colors c, float time=1)
    {
        Text.text = txt;
        Text.color = God.GetColor(c);
        TimeLeft = time;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0,1 * Time.deltaTime,0);
        TimeLeft -= Time.deltaTime;
        if (TimeLeft <= 0)
            Destroy(gameObject);
    }
}
